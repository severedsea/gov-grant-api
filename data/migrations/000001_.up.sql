-- households table
CREATE TABLE "public"."households" (
    "id" serial,
    "type" text NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
    PRIMARY KEY ("id")
);
COMMENT ON TABLE households IS 'Represents a household/housing unit';
COMMENT ON COLUMN households.type IS 'Household type (LANDED, CONDO, HDB)';

-- members table 
CREATE TABLE "public"."members" (
    "id" serial,
    "household_id" integer NOT NULL,
    "first_name" text NOT NULL,
    "last_name" text,
    "sex" text NOT NULL,
    "marital_status" text NOT NULL,
    "spouse_member_id" integer,
    "occupation_type" text NOT NULL,
    "annual_income_cents" integer NOT NULL,
    "dob" TIMESTAMP WITH TIME ZONE NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
    PRIMARY KEY ("id"),
    FOREIGN KEY ("household_id") REFERENCES "public"."households"("id") ON DELETE CASCADE,
    FOREIGN KEY ("spouse_member_id") REFERENCES "public"."members"("id") ON DELETE CASCADE
);
COMMENT ON TABLE members IS 'Represents an individual';
COMMENT ON COLUMN members.household_id IS 'ID for the household the member is part of';
COMMENT ON COLUMN members.first_name IS 'First name';
COMMENT ON COLUMN members.last_name IS 'Last name';
COMMENT ON COLUMN members.sex IS 'Sex (M, F)';
COMMENT ON COLUMN members.marital_status IS 'Marital status (SINGLE, MARRIED)';
COMMENT ON COLUMN members.spouse_member_id IS 'Represents the member''s spouse ID';
COMMENT ON COLUMN members.occupation_type IS 'Occupation type (UNEMPLOYED, STUDENT, EMPLOYED)';
COMMENT ON COLUMN members.annual_income_cents IS 'Annual in come in cents';
COMMENT ON COLUMN members.dob IS 'Date of birth';