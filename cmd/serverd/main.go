package main

import (
	"fmt"

	"gitlab.com/severedsea/golang-kit/logr"
	"gitlab.com/severedsea/golang-kit/web/server"
	"gitlab.com/severedsea/gov-grant-api/cmd/serverd/banner"
	"gitlab.com/severedsea/gov-grant-api/cmd/serverd/router"
	"gitlab.com/severedsea/gov-grant-api/internal/pkg/env"
)

func main() {
	banner.Print()

	// Initialise logger
	logr.Init(env.Get("LOG_LEVEL", "info"))

	// Start server
	s := server.New(fmt.Sprintf(":%s", env.Get("PORT", "3000")), router.Handler())
	s.Start()
}
