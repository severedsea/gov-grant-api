package apiweb

import (
	"net/http"

	"gitlab.com/severedsea/golang-kit/web" 
)

// Handler wraps the web.HandlerFunc to standard http.HandlerFunc with error handling
func Handler(h web.HandlerFunc) http.HandlerFunc {
	// Web handler
	wh := web.Handler{H: h}

	return wh.ServeHTTP
}
