// Package v1 contains serverd v1 API handlers
package v1

import (
	"github.com/go-chi/chi"
)

// Router registers handlers to the router provided in the argument
func Router(r chi.Router) {
	// Middlewares

	// Routes
	// TODO: Add RESTful version of the Graph resolvers
	// - Reuse internal/service/household package and
	// - Create respective response models within v1 package

	// h := HouseholdHandler{}
	// r.Get("/v1/households", apiweb.Handler(h.RetrieveAll))
	// r.Get("/v1/households/:id", apiweb.Handler(h.Retrieve))
	// r.Post("/v1/households", apiweb.Handler(m.Create))
	// r.Delete("/v1/households/:id", apiweb.Handler(h.Delete))
	// r.Get("/v1/households/grants", apiweb.Handler(h.SearchGrants))
	// r.Post("/v1/households/:id/members", apiweb.Handler(h.AddMember))

	// m := MemberHandler{}
	// r.Post("/v1/members/:id", apiweb.Handler(m.DeleteMember))
}
