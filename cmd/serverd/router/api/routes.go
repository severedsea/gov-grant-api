// Package api contains all api handlers
package api

import (
	"github.com/go-chi/chi"
	v1 "gitlab.com/severedsea/gov-grant-api/cmd/serverd/router/api/v1"
)

// Router registers handlers to the router provided in the argument
func Router(r chi.Router) {
	// Middlewares

	// Versioned routes
	r.Group(v1.Router)
}
