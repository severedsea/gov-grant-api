// Package router contains routing configuration for serverd
package router

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/cors"
	"gitlab.com/severedsea/golang-kit/web/middleware"
	"gitlab.com/severedsea/gov-grant-api/cmd/serverd/router/api"
	"gitlab.com/severedsea/gov-grant-api/cmd/serverd/router/graph"
)

// Handler returns the http handler that handles all requests
func Handler() http.Handler {
	r := chi.NewRouter()

	// Register base middlewares
	r.Use(middleware.Recover())
	r.Use(middleware.RequestLogger())

	// Basic CORS
	cors := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	})
	r.Use(cors.Handler)

	// API routes
	r.Group(api.Router)

	// Graph routes
	r.Group(graph.Router)

	return r
}
