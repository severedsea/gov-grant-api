package public

import (
	"context"

	"gitlab.com/severedsea/golang-kit/db"
	"gitlab.com/severedsea/gov-grant-api/internal/service/household"
)

func (r *queryResolver) SearchGrantsByHousehold(ctx context.Context, input *household.SearchInput) (*HouseholdGrantCollection, error) {
	if input == nil {
		input = &household.SearchInput{}
	}
	var slice []household.EligibleGrants
	if err := db.Tx(ctx, nil, func(tx db.Transactor) error {
		var err error
		slice, err = r.HouseholdService.SearchEligibleGrants(ctx, tx, *input)
		if err != nil {
			return err
		}
		return nil
	}, 3); err != nil {
		return nil, err
	}

	result := newHouseholdGrantCollection(slice)
	return &result, nil
}
