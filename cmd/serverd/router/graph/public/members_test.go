package public

import (
	"context"
	"testing"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/volatiletech/null/v8"
	"gitlab.com/severedsea/gov-grant-api/internal/models"
	"gitlab.com/severedsea/gov-grant-api/internal/service/household"
)

func TestAddHouseholdMember(t *testing.T) {
	// Given:
	ctx := context.Background()
	spouseMemberID := 111
	given := household.AddMemberInput{
		HouseholdID:    1,
		FirstName:      "member",
		SpouseMemberID: &spouseMemberID,
	}

	// Mocks
	m := &mockHouseholdService{}
	expectedCall := m.On("AddMember", mock.Anything, mock.Anything, given)
	mockResponse := models.Member{
		ID:             1,
		HouseholdID:    1,
		FirstName:      "member",
		SpouseMemberID: null.IntFrom(spouseMemberID),
	}
	mockResponse.R = mockResponse.R.NewStruct()
	mockResponse.R.SpouseMember = &models.Member{
		ID:          spouseMemberID,
		HouseholdID: 1,
		FirstName:   "spouse",
	}
	expectedCall.Return(mockResponse, nil)

	r := mutationResolver{
		Resolver: &Resolver{
			HouseholdService: m,
		},
	}

	// When:
	result, err := r.AddHouseholdMember(ctx, given)

	// Then:
	require.NoError(t, err)
	assertMemberEdge(t, mockResponse, result)
}

func TestDeleteHouseholdMember(t *testing.T) {
	// Given:
	ctx := context.Background()
	given := 111

	// Mocks
	m := &mockHouseholdService{}
	expectedCall := m.On("DeleteMember", mock.Anything, mock.Anything, given)
	expectedCall.Return(nil)

	r := mutationResolver{
		Resolver: &Resolver{
			HouseholdService: m,
		},
	}

	// When:
	_, err := r.DeleteHouseholdMember(ctx, given)

	// Then:
	require.NoError(t, err)
}

func assertMemberCollection(t *testing.T, exp models.MemberSlice, act *MemberCollection) {
	edges := act.Edges()
	require.Equal(t, len(exp), len(edges))
	for i, it := range exp {
		assertMemberEdge(t, *it, &edges[i])
	}
}

func assertMemberEdge(t *testing.T, exp models.Member, act *MemberEdge) {
	node := act.Node()
	require.Equal(t, node.ID(), exp.ID)
	require.Equal(t, node.HouseholdID(), exp.HouseholdID)
	require.Equal(t, node.FirstName(), exp.FirstName)
	require.Equal(t, node.DateOfBirth(), exp.Dob)
	require.Equal(t, node.AnnualIncomeCents(), exp.AnnualIncomeCents)
	require.Equal(t, node.LastName(), exp.LastName.Ptr())
	require.Equal(t, node.Sex(), household.MemberSex(exp.Sex))
	if exp.R != nil && exp.R.SpouseMember != nil {
		require.Equal(t, node.Spouse().node.model, *exp.R.SpouseMember)
	}
	require.Equal(t, node.MaritalStatus(), household.MemberMaritalStatus(exp.MaritalStatus))
	require.Equal(t, node.OccupationType(), household.MemberOccupationType(exp.OccupationType))
}
