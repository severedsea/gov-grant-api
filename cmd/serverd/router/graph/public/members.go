package public

import (
	"context"

	"gitlab.com/severedsea/golang-kit/db"
	"gitlab.com/severedsea/gov-grant-api/internal/models"
	"gitlab.com/severedsea/gov-grant-api/internal/service/household"
)

func (r *mutationResolver) AddHouseholdMember(ctx context.Context, input household.AddMemberInput) (*MemberEdge, error) {
	var model models.Member
	if err := db.Tx(ctx, nil, func(tx db.Transactor) error {
		var err error
		model, err = r.HouseholdService.AddMember(ctx, tx, input)
		if err != nil {
			return err
		}
		return nil
	}, 3); err != nil {
		return nil, err
	}

	result := newMemberEdge(model)
	return &result, nil
}

func (r *mutationResolver) DeleteHouseholdMember(ctx context.Context, id int) (bool, error) {
	if err := db.Tx(ctx, nil, func(tx db.Transactor) error {
		err := r.HouseholdService.DeleteMember(ctx, tx, id)
		if err != nil {
			return err
		}
		return nil
	}, 3); err != nil {
		return false, err
	}

	return true, nil
}
