package public

import (
	"time"

	"gitlab.com/severedsea/gov-grant-api/internal/models"
	"gitlab.com/severedsea/gov-grant-api/internal/service/household"
)

// newMemberEdge returns a new MemberEdge GraphQL type instance
func newMemberEdge(m models.Member) MemberEdge {
	return MemberEdge{
		node: Member{
			model: m,
		},
	}
}

// newMemberCollection returns a new MemberCollection GraphQL type instance
func newMemberCollection(slice models.MemberSlice) MemberCollection {
	var result []MemberEdge
	for _, m := range slice {
		result = append(result, newMemberEdge(*m))
	}
	return MemberCollection{edges: result}
}

// Member graphql type
type Member struct {
	model models.Member
}

// ID is the member ID
func (t *Member) ID() int {
	return t.model.ID
}

// HouseholdID is the household ID this member is part of
func (t *Member) HouseholdID() int {
	return t.model.HouseholdID
}

// FirstName is the member first name
func (t *Member) FirstName() string {
	return t.model.FirstName
}

// LastName is the member last name
func (t *Member) LastName() *string {
	return t.model.LastName.Ptr()
}

// Sex is the member's sex
func (t *Member) Sex() household.MemberSex {
	return household.MemberSex(t.model.Sex)
}

// MaritalStatus is the member's marital status
func (t *Member) MaritalStatus() household.MemberMaritalStatus {
	return household.MemberMaritalStatus(t.model.MaritalStatus)
}

// Spouse is the member's spouse
func (t *Member) Spouse() *MemberEdge {
	if t.model.R == nil || t.model.R.SpouseMember == nil {
		return nil
	}
	result := newMemberEdge(*t.model.R.SpouseMember)
	return &result
}

// OccupationType is the member's occupation type
func (t *Member) OccupationType() household.MemberOccupationType {
	return household.MemberOccupationType(t.model.OccupationType)
}

// AnnualIncomeCents is the member's annual income in cents
func (t *Member) AnnualIncomeCents() int {
	return t.model.AnnualIncomeCents
}

// DateOfBirth is the member's date of birth
func (t *Member) DateOfBirth() time.Time {
	return t.model.Dob
}

// MemberCollection graphql type
type MemberCollection struct {
	edges []MemberEdge
}

// Edges returns the list of edges
func (t *MemberCollection) Edges() []MemberEdge {
	return t.edges
}

// MemberEdge graphql type
type MemberEdge struct {
	node Member
}

// Node returns the member node
func (t *MemberEdge) Node() Member {
	return t.node
}
