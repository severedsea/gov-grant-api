package public

import (
	"gitlab.com/severedsea/gov-grant-api/internal/service/household"
)

// newHouseholdGrantEdge returns a new HouseholdGrantEdge GraphQL type instance
func newHouseholdGrantEdge(m household.EligibleGrants) HouseholdGrantEdge {
	return HouseholdGrantEdge{
		node: HouseholdGrant{
			model: m,
		},
	}
}

// newHouseholdGrantCollection returns a new HouseholdGrantCollection GraphQL type instance
func newHouseholdGrantCollection(slice []household.EligibleGrants) HouseholdGrantCollection {
	var result []HouseholdGrantEdge
	for _, m := range slice {
		result = append(result, newHouseholdGrantEdge(m))
	}
	return HouseholdGrantCollection{edges: result}
}

// HouseholdGrant is the GraphQL type to represent an HouseholdGrant
type HouseholdGrant struct {
	model household.EligibleGrants
}

// Household contains the household information
func (t *HouseholdGrant) Household() HouseholdEdge {
	return newHouseholdEdge(t.model.Household)
}

// Grants contains the eligible grants for the household
func (t *HouseholdGrant) Grants() []string {
	return t.model.Grants
}

// HouseholdGrantCollection is a Collection of HouseholdGrant types
type HouseholdGrantCollection struct {
	edges []HouseholdGrantEdge
}

// Edges returns the household edges
func (t *HouseholdGrantCollection) Edges() []HouseholdGrantEdge {
	return t.edges
}

// HouseholdGrantEdge is HouseholdGrant type wrapped in `Edge`
type HouseholdGrantEdge struct {
	node HouseholdGrant
}

// Node is the household node
func (t *HouseholdGrantEdge) Node() HouseholdGrant {
	return t.node
}
