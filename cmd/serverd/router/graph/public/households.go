package public

import (
	"context"

	"gitlab.com/severedsea/golang-kit/db"
	"gitlab.com/severedsea/gov-grant-api/internal/models"
	"gitlab.com/severedsea/gov-grant-api/internal/service/household"
)

func (r *mutationResolver) CreateHousehold(ctx context.Context, input household.CreateInput) (*HouseholdEdge, error) {
	var model models.Household
	if err := db.Tx(ctx, nil, func(tx db.Transactor) error {
		var err error
		model, err = r.HouseholdService.Create(ctx, tx, input)
		if err != nil {
			return err
		}
		return nil
	}, 3); err != nil {
		return nil, err
	}

	result := newHouseholdEdge(model)
	return &result, nil
}

func (r *queryResolver) Household(ctx context.Context, id int) (*HouseholdEdge, error) {
	var model models.Household
	if err := db.Tx(ctx, nil, func(tx db.Transactor) error {
		var err error
		model, err = r.HouseholdService.Retrieve(ctx, tx, id)
		if err != nil {
			return err
		}
		return nil
	}, 3); err != nil {
		return nil, err
	}

	result := newHouseholdEdge(model)
	return &result, nil
}

func (r *queryResolver) Households(ctx context.Context) (*HouseholdCollection, error) {
	var slice models.HouseholdSlice
	if err := db.Tx(ctx, nil, func(tx db.Transactor) error {
		var err error
		slice, err = r.HouseholdService.RetrieveAll(ctx, tx)
		if err != nil {
			return err
		}
		return nil
	}, 3); err != nil {
		return nil, err
	}

	result := newHouseholdCollection(slice)
	return &result, nil
}

func (r *mutationResolver) DeleteHousehold(ctx context.Context, id int) (bool, error) {
	if err := db.Tx(ctx, nil, func(tx db.Transactor) error {
		err := r.HouseholdService.Delete(ctx, tx, id)
		if err != nil {
			return err
		}
		return nil
	}, 3); err != nil {
		return false, err
	}

	return true, nil
}
