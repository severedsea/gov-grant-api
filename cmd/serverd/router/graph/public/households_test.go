package public

import (
	"context"
	"testing"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/severedsea/gov-grant-api/internal/models"
	"gitlab.com/severedsea/gov-grant-api/internal/service/household"
)

func TestCreateHousehold(t *testing.T) {
	// Given:
	ctx := context.Background()
	given := household.CreateInput{
		Type: household.TypeHDB,
	}

	// Mocks
	m := &mockHouseholdService{}
	expectedCall := m.On("Create", mock.Anything, mock.Anything, given)
	mockResponse := models.Household{
		ID:   1,
		Type: "HDB",
	}
	expectedCall.Return(mockResponse, nil)

	r := mutationResolver{
		Resolver: &Resolver{
			HouseholdService: m,
		},
	}

	// When:
	result, err := r.CreateHousehold(ctx, given)

	// Then:
	require.NoError(t, err)
	assertHouseholdEdge(t, mockResponse, result)
}

func TestHousehold(t *testing.T) {
	// Given:
	ctx := context.Background()
	given := 1

	// Mocks
	m := &mockHouseholdService{}
	expectedCall := m.On("Retrieve", mock.Anything, mock.Anything, given)
	mockResponse := models.Household{
		ID:   1,
		Type: "HDB",
	}
	mockResponse.R = mockResponse.R.NewStruct()
	mockResponse.R.Members = models.MemberSlice{
		{ID: 11},
		{ID: 22},
	}
	expectedCall.Return(mockResponse, nil)

	r := queryResolver{
		Resolver: &Resolver{
			HouseholdService: m,
		},
	}

	// When:
	result, err := r.Household(ctx, given)

	// Then:
	require.NoError(t, err)
	assertHouseholdEdge(t, mockResponse, result)
}

func TestHouseholds(t *testing.T) {
	// Given:
	ctx := context.Background()

	// Mocks
	m := &mockHouseholdService{}
	expectedCall := m.On("RetrieveAll", mock.Anything, mock.Anything)
	mockResponse := models.HouseholdSlice{
		{ID: 1, Type: "HDB"},
		{ID: 2, Type: "CONDO"},
	}
	for i := range mockResponse {
		mockResponse[i].R = mockResponse[i].R.NewStruct()
		mockResponse[i].R.Members = models.MemberSlice{
			{ID: 11},
			{ID: 22},
		}
	}
	expectedCall.Return(mockResponse, nil)

	r := queryResolver{
		Resolver: &Resolver{
			HouseholdService: m,
		},
	}

	// When:
	result, err := r.Households(ctx)

	// Then:
	require.NoError(t, err)
	assertHouseholdCollection(t, mockResponse, result)
}

func TestDeleteHousehold(t *testing.T) {
	// Given:
	ctx := context.Background()
	given := 111

	// Mocks
	m := &mockHouseholdService{}
	expectedCall := m.On("Delete", mock.Anything, mock.Anything, given)
	expectedCall.Return(nil)

	r := mutationResolver{
		Resolver: &Resolver{
			HouseholdService: m,
		},
	}

	// When:
	_, err := r.DeleteHousehold(ctx, given)

	// Then:
	require.NoError(t, err)
}

func assertHouseholdCollection(t *testing.T, exp models.HouseholdSlice, act *HouseholdCollection) {
	edges := act.Edges()
	require.Equal(t, len(exp), len(edges))
	for i, it := range exp {
		assertHouseholdEdge(t, *it, &edges[i])
	}
}

func assertHouseholdEdge(t *testing.T, exp models.Household, act *HouseholdEdge) {
	node := act.Node()
	require.Equal(t, exp.ID, node.ID())
	require.Equal(t, household.Type(exp.Type), node.Type())
	if exp.R != nil {
		require.Equal(t, len(exp.R.Members), len(node.Members().Edges()))
		for i, it := range exp.R.Members {
			assertMemberEdge(t, *it, &node.Members().Edges()[i])
		}
	}
}
