package public

import (
	"context"
	"testing"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/severedsea/gov-grant-api/internal/models"
	"gitlab.com/severedsea/gov-grant-api/internal/service/household"
)

func TestSearchGrantsByHousehold(t *testing.T) {
	// Given:
	ctx := context.Background()
	totalIncomeCents := 15000000
	given := household.SearchInput{
		TotalIncomeCents: &totalIncomeCents,
	}

	// Mocks
	m := &mockHouseholdService{}
	expectedCall := m.On("SearchEligibleGrants", mock.Anything, mock.Anything, given)
	mockResponse := []household.EligibleGrants{
		{Household: models.Household{ID: 1, Type: "HDB"}, Grants: []string{"1", "2"}},
		{Household: models.Household{ID: 2, Type: "CONDO"}, Grants: []string{"3", "4"}},
	}
	expectedCall.Return(mockResponse, nil)

	r := queryResolver{
		Resolver: &Resolver{
			HouseholdService: m,
		},
	}

	// When:
	result, err := r.SearchGrantsByHousehold(ctx, &given)

	// Then:
	require.NoError(t, err)
	assertHouseholdGrantCollection(t, mockResponse, result)
}

func assertHouseholdGrantCollection(t *testing.T, exp []household.EligibleGrants, act *HouseholdGrantCollection) {
	edges := act.Edges()
	require.Equal(t, len(exp), len(edges))
	for i, it := range exp {
		assertHouseholdGrantEdge(t, it, &edges[i])
	}
}

func assertHouseholdGrantEdge(t *testing.T, exp household.EligibleGrants, act *HouseholdGrantEdge) {
	node := act.Node()
	h := node.Household()
	assertHouseholdEdge(t, exp.Household, &h)
	require.Equal(t, exp.Grants, node.Grants())
}
