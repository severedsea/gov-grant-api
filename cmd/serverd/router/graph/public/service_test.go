package public

import (
	"context"

	"github.com/stretchr/testify/mock"
	"gitlab.com/severedsea/golang-kit/db"
	"gitlab.com/severedsea/gov-grant-api/internal/models"
	"gitlab.com/severedsea/gov-grant-api/internal/service/household"
)

// -------------------------------
// Mocks
// -------------------------------

// --- Household Service ---

type mockHouseholdService struct {
	mock.Mock
}

func (m *mockHouseholdService) Create(ctx context.Context, tx db.Transactor, input household.CreateInput) (models.Household, error) {
	args := m.Called(ctx, tx, input)
	return args.Get(0).(models.Household), args.Error(1)
}

func (m *mockHouseholdService) Retrieve(ctx context.Context, tx db.Transactor, id int) (models.Household, error) {
	args := m.Called(ctx, tx, id)
	return args.Get(0).(models.Household), args.Error(1)
}
func (m *mockHouseholdService) Delete(ctx context.Context, tx db.Transactor, id int) error {
	args := m.Called(ctx, tx, id)
	return args.Error(0)
}

func (m *mockHouseholdService) RetrieveAll(ctx context.Context, tx db.Transactor) (models.HouseholdSlice, error) {
	args := m.Called(ctx, tx)
	return args.Get(0).(models.HouseholdSlice), args.Error(1)
}

func (m *mockHouseholdService) SearchEligibleGrants(ctx context.Context, tx db.Transactor, input household.SearchInput) ([]household.EligibleGrants, error) {
	args := m.Called(ctx, tx, input)
	return args.Get(0).([]household.EligibleGrants), args.Error(1)
}

func (m *mockHouseholdService) AddMember(ctx context.Context, tx db.Transactor, input household.AddMemberInput) (models.Member, error) {
	args := m.Called(ctx, tx, input)
	return args.Get(0).(models.Member), args.Error(1)
}

func (m *mockHouseholdService) DeleteMember(ctx context.Context, tx db.Transactor, id int) error {
	args := m.Called(ctx, tx, id)
	return args.Error(0)
}
