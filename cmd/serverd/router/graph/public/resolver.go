package public

import "gitlab.com/severedsea/gov-grant-api/internal/service/household"

// NewConfig returns the GraphQL config
func NewConfig() Config {
	c := Config{
		Resolvers: &Resolver{
			HouseholdService: household.New(),
		},
	}
	return c
}

// Resolver struct
type Resolver struct {
	HouseholdService HouseholdService
}

// Query returns the query resolver
func (r *Resolver) Query() QueryResolver {
	return &queryResolver{r}
}

// Mutation returns the mutation resolver
func (r *Resolver) Mutation() MutationResolver {
	return &mutationResolver{r}
}

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
