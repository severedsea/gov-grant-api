package public

import (
	"gitlab.com/severedsea/gov-grant-api/internal/models"
	"gitlab.com/severedsea/gov-grant-api/internal/service/household"
)

// newHouseholdEdge returns a new HouseholdEdge GraphQL type instance
func newHouseholdEdge(m models.Household) HouseholdEdge {
	return HouseholdEdge{
		node: Household{
			model: m,
		},
	}
}

// newHouseholdCollection returns a new HouseholdCollection GraphQL type instance
func newHouseholdCollection(slice models.HouseholdSlice) HouseholdCollection {
	var result []HouseholdEdge
	for _, m := range slice {
		result = append(result, newHouseholdEdge(*m))
	}
	return HouseholdCollection{edges: result}
}

// Household GraphQL type
type Household struct {
	model models.Household
}

// ID is the household identifier
func (t *Household) ID() int {
	return t.model.ID
}

// Type is the household type
func (t *Household) Type() household.Type {
	return household.Type(t.model.Type)
}

// Members returns the household members
func (t *Household) Members() *MemberCollection {
	if t.model.R == nil || len(t.model.R.Members) <= 0 {
		return nil
	}
	v := newMemberCollection(t.model.R.Members)
	return &v
}

// HouseholdCollection is GraphQL type representing a list of households
type HouseholdCollection struct {
	edges []HouseholdEdge
}

// Edges returns the household edges
func (t *HouseholdCollection) Edges() []HouseholdEdge {
	return t.edges
}

// HouseholdEdge is the GraphQL type representing a Household edge
type HouseholdEdge struct {
	node Household
}

// Node is the household node
func (t *HouseholdEdge) Node() Household {
	return t.node
}
