package public

import (
	"context"

	"gitlab.com/severedsea/golang-kit/db"
	"gitlab.com/severedsea/gov-grant-api/internal/models"
	"gitlab.com/severedsea/gov-grant-api/internal/service/household"
)

// HouseholdService has household-related services with specialised business logic and validations
type HouseholdService interface {
	Create(ctx context.Context, tx db.Transactor, input household.CreateInput) (models.Household, error)
	Delete(ctx context.Context, tx db.Transactor, id int) error
	Retrieve(ctx context.Context, tx db.Transactor, id int) (models.Household, error)
	RetrieveAll(ctx context.Context, tx db.Transactor) (models.HouseholdSlice, error)
	SearchEligibleGrants(ctx context.Context, tx db.Transactor, input household.SearchInput) ([]household.EligibleGrants, error)

	AddMember(ctx context.Context, tx db.Transactor, input household.AddMemberInput) (models.Member, error)
	DeleteMember(ctx context.Context, tx db.Transactor, id int) error
}
