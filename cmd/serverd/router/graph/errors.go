package graph

import (
	"context"

	"github.com/99designs/gqlgen/graphql"
	"github.com/99designs/gqlgen/handler"
	"github.com/vektah/gqlparser/gqlerror"
	"gitlab.com/severedsea/golang-kit/web"
)

// webErrorPresenter returns an ErrorPresenterFunc (Option) for web.Error
func webErrorPresenter() handler.Option {
	return handler.ErrorPresenter(
		func(ctx context.Context, e error) *gqlerror.Error {

			err, ok := e.(*web.Error)
			if !ok {
				err = web.NewError(e, e.Error())
			}

			e = &gqlerror.Error{
				Message: err.Error(),
				Path:    graphql.GetResolverContext(ctx).Path(),
				Extensions: map[string]interface{}{
					"status":            err.Status,
					"error":             err.Code,
					"error_description": err.Desc,
				},
			}
			return graphql.DefaultErrorPresenter(ctx, e)
		},
	)
}
