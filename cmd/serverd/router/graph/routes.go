package graph

import (
	"os"

	"github.com/99designs/gqlgen/handler"
	"github.com/go-chi/chi"
	"gitlab.com/severedsea/golang-kit/web/gqlgen"
	"gitlab.com/severedsea/gov-grant-api/cmd/serverd/router/graph/public"
)

// Router registers GraphQL handlers to the router provided in the argument
func Router(r chi.Router) {
	r.Group(publicRoutes)
}

// Public schema routes
func publicRoutes(r chi.Router) {
	// Disable playground on PROD
	if os.Getenv("APP_ENV") != "prod" {
		r.Handle("/graphiql", handler.Playground("Public", "/graphql"))
	}

	r.Group(func(r chi.Router) {
		r.Handle("/graphql", handler.GraphQL(
			public.NewExecutableSchema(public.NewConfig()),
			gqlgen.RequestLoggerMiddleware(),
			gqlgen.ResolverLoggerMiddleware(),
			gqlgen.WebErrorPresenter(),
		))
	})
}
