# gov-grant-api

API service that provides logic on government grant eligibility

## Documentation

- [Requirements](docs/requirements.md)
- [API](docs/api.md)
- [Design Decisions and Assumptions](docs/design.md)

## Setup

### Prerequisites

- [Docker](https://www.docker.com/get-started) - 17.12.0+
- [Docker Compose](https://docs.docker.com/compose/install/) - 1.25+
- [GNU Make](https://www.gnu.org/software/make/) - 3.81+

### Pipeline

- `Makefile` to execute targets to run, build, test, and teardown local environment
- `docker-compose` to run containers for running, building, executing tests, executing database migrations, and running services (i.e. database)

### Running Locally

```
$ make run
```

Default port is [3000](.env)

### Teardown

```
$ make compose-down
```

## Development

### Prerequisites

- [SQLBoiler](https://github.com/volatiletech/sqlboiler) - 4.0.0+

### Test

Executes all the tests

```
$ make test
```

### Build

Builds `serverd` into a binary

```
$ make build
```

### Generate boilerplate

Generates boilerplate code such as models and GraphQL engine

```
$ make boilerplate
```

### Redo database migration

Useful when making changes to database schema

```
$ make dbredo
```
