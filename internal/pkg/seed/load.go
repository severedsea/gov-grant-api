package seed

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/severedsea/golang-kit/db"
	"gitlab.com/severedsea/gov-grant-api/internal/pkg/projectpath"
)

// LoadTestDataFile load test data from a file
func LoadTestDataFile(t *testing.T, tx db.Transactor, filename string) {
	body, err := projectpath.Read(filename)
	require.NoError(t, err)

	_, err = tx.Exec(string(body))
	require.NoError(t, err)
}
