package gqlscalar

import (
	"fmt"
	"io"
	"reflect"
	"strconv"

	"github.com/pkg/errors"
)

// Int64 represents a custom GraphQL "Int64" scalar type
type Int64 string

// UnmarshalGQL unmarshals Int64, a custom scalar type
// Implements graphql.Marshaler
func (i *Int64) UnmarshalGQL(v interface{}) error {
	var err error
	switch input := v.(type) {
	case string:
		*i = Int64(input)
	case int64:
		*i = Int64(strconv.FormatInt(int64(input), 10))
	default:
		err = errors.Errorf("Int64 must be of string type, but got %s instead", reflect.TypeOf(input))
	}
	return err
}

// MarshalGQL marshal Int64, required by gqlgen
// Implements graphql.Marshaler
func (i Int64) MarshalGQL(w io.Writer) {
	fmt.Fprint(w, strconv.Quote(string(i)))
}

// ToGo converts the custom scalar type to Go type
func (i *Int64) ToGo() (int64, error) {
	if i == nil {
		return 0, nil
	}
	return strconv.ParseInt(string(*i), 10, 64)
}

// NewInt64 takes an int64 type and converts it to custom Int64 scalar type
func NewInt64(i int64) Int64 {
	return Int64(strconv.FormatInt(int64(i), 10))
}
