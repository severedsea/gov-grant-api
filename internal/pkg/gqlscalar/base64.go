package gqlscalar

import (
	"encoding/base64"
	"fmt"
	"io"
	"reflect"
	"strconv"

	"github.com/pkg/errors"
)

// Base64 represents a custom GraphQL "Base64" scalar type
type Base64 string

// UnmarshalGQL unmarshals Base64, a custom scalar type
// Implements graphql.Marshaler
func (i *Base64) UnmarshalGQL(v interface{}) error {
	var err error
	switch input := v.(type) {
	case string:
		*i = Base64(input)
	case []byte:
		*i = Base64(base64.StdEncoding.EncodeToString(input))
	default:
		err = errors.Errorf("Base64 must be of string type, but got %s instead", reflect.TypeOf(input))
	}
	return err
}

// MarshalGQL marshal Base64, required by gqlgen
// Implements graphql.Marshaler
func (i Base64) MarshalGQL(w io.Writer) {
	fmt.Fprint(w, strconv.Quote(string(i)))
}

// ToGo converts the custom scalar type to Go type
func (i *Base64) ToGo() ([]byte, error) {
	if i == nil {
		return []byte{}, nil
	}
	return base64.StdEncoding.DecodeString(string(*i))
}

// NewBase64 returns a Base64 instance from the provided []byte
func NewBase64(src []byte) Base64 {
	return Base64(base64.StdEncoding.EncodeToString(src))
}
