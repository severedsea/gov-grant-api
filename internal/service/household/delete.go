package household

import (
	"context"

	"gitlab.com/severedsea/golang-kit/db"
	"gitlab.com/severedsea/golang-kit/web"
	"gitlab.com/severedsea/gov-grant-api/internal/models"
)

// Delete deletes a household
func (Service) Delete(ctx context.Context, tx db.Transactor, id int) error {
	deleted, err := models.Households(
		models.HouseholdWhere.ID.EQ(id),
	).DeleteAll(ctx, tx)
	if err != nil {
		return web.NewError(ErrDatabase, err.Error())
	}
	if deleted <= 0 {
		return ErrHouseholdNotFound
	}
	return nil
}
