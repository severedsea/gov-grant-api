package household

// Type is the named type for household types
type Type string

// Household types
const (
	TypeLanded Type = "LANDED"
	TypeCondo  Type = "CONDO"
	TypeHDB    Type = "HDB"
)

// IsValid return true if the household type is valid
func (t Type) IsValid() bool {
	switch t {
	case TypeLanded, TypeCondo, TypeHDB:
		return true
	}
	return false
}

// MemberSex is the named type for a member's sex
type MemberSex string

// Member sex
const (
	MemberSexM MemberSex = "M"
	MemberSexF MemberSex = "F"
)

// IsValid return true if the member sex is valid
func (t MemberSex) IsValid() bool {
	switch t {
	case MemberSexM, MemberSexF:
		return true
	}
	return false
}

// MemberMaritalStatus is the named type for a member's marital status
type MemberMaritalStatus string

// Member marital status
const (
	MemberMaritalStatusSingle  MemberMaritalStatus = "SINGLE"
	MemberMaritalStatusMarried MemberMaritalStatus = "MARRIED"
)

// IsValid return true if the marital status is valid
func (t MemberMaritalStatus) IsValid() bool {
	switch t {
	case MemberMaritalStatusSingle, MemberMaritalStatusMarried:
		return true
	}
	return false
}

// MemberOccupationType is the named type for a member's marital status
type MemberOccupationType string

// Member occupation
const (
	MemberOccupationTypeUnemployed MemberOccupationType = "UNEMPLOYED"
	MemberOccupationTypeStudent    MemberOccupationType = "STUDENT"
	MemberOccupationTypeEmployed   MemberOccupationType = "EMPLOYED"
)

// IsValid return true if the marital status is valid
func (t MemberOccupationType) IsValid() bool {
	switch t {
	case MemberOccupationTypeUnemployed, MemberOccupationTypeStudent, MemberOccupationTypeEmployed:
		return true
	}
	return false
}
