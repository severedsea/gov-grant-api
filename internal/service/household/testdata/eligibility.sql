-- households
INSERT INTO "public"."households"("id", "type", "created_at", "updated_at") VALUES
(9000, 'LANDED', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
(9001, 'LANDED', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
(9002, 'HDB', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
(9003, 'CONDO', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
(9004, 'HDB', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);


-- Student Encouragement Bonus, Baby Sunshine Grant
-- 9000
INSERT INTO "public"."members" ("id", "household_id", "first_name", "last_name", "sex", "marital_status", "occupation_type", "annual_income_cents", "dob", "created_at", "updated_at") VALUES 
(90001, 9000, 'seb_lessthan16', 'lastname', 'M', 'SINGLE', 'STUDENT', 0, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
(90002, 9000, 'seb_parent', 'lastname', 'M', 'SINGLE', 'EMPLOYED', 14999999, CURRENT_TIMESTAMP - interval '30 years', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

-- Family Togetherness Scheme, Baby Sunshine Grant
-- 9001
INSERT INTO "public"."members" ("id", "household_id", "first_name", "last_name", "sex", "marital_status", "spouse_member_id", "occupation_type", "annual_income_cents", "dob", "created_at", "updated_at") VALUES 
(90011, 9001, 'fts_lessthan18', 'lastname', 'M', 'SINGLE', NULL, 'STUDENT', 0, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
(90012, 9001, 'fts_husband', 'lastname', 'M', 'MARRIED', 90013,'EMPLOYED', 99999999, CURRENT_TIMESTAMP - interval '30 years', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
(90013, 9001, 'fts_wife', 'lastname', 'F', 'MARRIED',90012, 'EMPLOYED', 99999999, CURRENT_TIMESTAMP - interval '30 years', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

-- Elder Bonus, YOLO GST Grant
-- 9002
INSERT INTO "public"."members" ("id", "household_id", "first_name", "last_name", "sex", "marital_status", "occupation_type", "annual_income_cents", "dob", "created_at", "updated_at") VALUES 
(90021, 9002, 'eb_greaterthan50', 'lastname', 'M', 'SINGLE', 'UNEMPLOYED', 0, CURRENT_TIMESTAMP - interval '51 years', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

-- Baby Sunshine Grant, Student Encouragement Bonus
-- 9003
INSERT INTO "public"."members" ("id", "household_id", "first_name", "last_name", "sex", "marital_status", "occupation_type", "annual_income_cents", "dob", "created_at", "updated_at") VALUES 
(90031, 9003, 'bsg_lessthan5', 'lastname', 'M', 'SINGLE', 'EMPLOYED', 10100000, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

-- YOLO GST Grant, 
-- 9004
INSERT INTO "public"."members" ("id", "household_id", "first_name", "last_name", "sex", "marital_status", "occupation_type", "annual_income_cents", "dob", "created_at", "updated_at") VALUES 
(90041, 9004, 'ygg_noincome', 'lastname', 'M', 'SINGLE', 'UNEMPLOYED', 0, CURRENT_TIMESTAMP - interval '30 years', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);