package household

import (
	"context"

	"github.com/volatiletech/sqlboiler/v4/boil"
	"gitlab.com/severedsea/golang-kit/db"
	"gitlab.com/severedsea/golang-kit/web"
	"gitlab.com/severedsea/gov-grant-api/internal/models"
)

// CreateInput represents the input struct for creating a household
type CreateInput struct {
	Type Type
}

// Validate validates the input values
func (i *CreateInput) validate() error {
	if !i.Type.IsValid() {
		return ErrInvalidType
	}
	return nil
}

// ToModel converts the input to model
func (i *CreateInput) toModel() models.Household {
	result := models.Household{
		Type: string(i.Type),
	}
	return result
}

// Create creates a household
func (Service) Create(ctx context.Context, tx db.Transactor, input CreateInput) (models.Household, error) {
	// Validate input
	if err := input.validate(); err != nil {
		return models.Household{}, err
	}

	// Convert to model
	m := input.toModel()

	// Insert record
	if err := m.Insert(ctx, tx, boil.Infer()); err != nil {
		return models.Household{}, web.NewError(ErrDatabase, err.Error())
	}

	return m, nil
}
