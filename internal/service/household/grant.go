package household

import (
	"time"

	"gitlab.com/severedsea/gov-grant-api/internal/models"
)

// Grants
var (
	StudentEncouragementBonus Grant = studentEncouragementBonus{}
	FamilyTogethernessScheme  Grant = familyTogethernessScheme{}
	ElderBonus                Grant = elderBonus{}
	BabySunshineGrant         Grant = babySunshineGrant{}
	YoloGSTGrant              Grant = yoloGSTGrant{}
)

// Grant is the interface for a Grant
type Grant interface {
	Name() string
	IsEligible(h models.Household, now time.Time) (bool, error)
}

// studentEncouragementBonus contains household eligibility rules
type studentEncouragementBonus struct{}

func (g studentEncouragementBonus) Name() string {
	return "Student Encouragement Bonus"
}

func (g studentEncouragementBonus) IsEligible(h models.Household, now time.Time) (bool, error) {
	// Eligiblity: Household income of less than $150,000.
	income := sumIncome(h)
	if income >= 15000000 {
		return false, nil
	}

	// Eligibility: Households with children of less than 16 years old.
	if !hasMemberAgeLessThan(h, 16, now) {
		return false, nil
	}

	return true, nil
}

// familyTogethernessScheme contains household eligibility rules
type familyTogethernessScheme struct{}

func (g familyTogethernessScheme) Name() string {
	return "Family Togetherness Scheme"
}

func (g familyTogethernessScheme) IsEligible(h models.Household, now time.Time) (bool, error) {
	// Eligibility: Households with husband & wife
	if !hasCouple(h) {
		return false, nil
	}

	// Eligibility: Has child(ren) younger than 18 years old.
	if !hasMemberAgeLessThan(h, 18, now) {
		return false, nil
	}

	return true, nil
}

// elderBonus contains household eligibility rules
type elderBonus struct{}

func (g elderBonus) Name() string {
	return "Elder Bonus"
}

func (g elderBonus) IsEligible(h models.Household, now time.Time) (bool, error) {
	// Eligibility: HDB household with family members above the age of 50.
	if Type(h.Type) != TypeHDB {
		return false, nil
	}

	if !hasMemberAgeGreaterThan(h, 50, now) {
		return false, nil
	}

	return true, nil
}

// babySunshineGrant contains household eligibility rules
type babySunshineGrant struct{}

func (g babySunshineGrant) Name() string {
	return "Baby Sunshine Grant"
}

func (g babySunshineGrant) IsEligible(h models.Household, now time.Time) (bool, error) {
	// Eligibility: Household with young children younger than 5.
	if !hasMemberAgeLessThan(h, 5, now) {
		return false, nil
	}

	return true, nil
}

// yoloGSTGrant contains household eligibility rules
type yoloGSTGrant struct{}

func (g yoloGSTGrant) Name() string {
	return "YOLO GST Grant"
}

func (g yoloGSTGrant) IsEligible(h models.Household, now time.Time) (bool, error) {
	// Eligibility: HDB households with annual income of less than $100,000.
	if Type(h.Type) != TypeHDB {
		return false, nil
	}
	income := sumIncome(h)
	if income >= 10000000 {
		return false, nil
	}

	return true, nil
}

// sumIncome sums the income of all members in the household
func sumIncome(h models.Household) int {
	var sum int
	forEachMember(h, func(m *models.Member) bool {
		sum += m.AnnualIncomeCents
		return true
	})
	return sum
}

func hasCouple(h models.Household) bool {
	members := map[int]bool{}
	spouses := map[int]bool{}
	forEachMember(h, func(m *models.Member) bool {
		members[m.ID] = true
		if m.SpouseMemberID.Valid {
			spouses[m.SpouseMemberID.Int] = true
		}
		return true
	})

	// If any of member's registered spouse is also member of this household, return true
	for k := range spouses {
		if _, ok := members[k]; ok {
			return true
		}
	}
	return false
}

func hasMemberAgeLessThan(h models.Household, years int, now time.Time) bool {
	var found bool
	forEachMember(h, func(m *models.Member) bool {
		threshold := time.Date(m.Dob.Year()+years, m.Dob.Month(), m.Dob.Day(), 0, 0, 0, 0, time.UTC)
		if now.UTC().Before(threshold) {
			found = true
			return false
		}
		return true
	})
	return found
}

func hasMemberAgeGreaterThan(h models.Household, years int, now time.Time) bool {
	var found bool
	forEachMember(h, func(m *models.Member) bool {
		threshold := time.Date(m.Dob.Year()+years, m.Dob.Month(), m.Dob.Day(), 0, 0, 0, 0, time.UTC)
		if now.UTC().After(threshold) {
			found = true
			return false
		}
		return true
	})
	return found
}

func forEachMember(h models.Household, f func(m *models.Member) bool) {
	if h.R == nil {
		return
	}
	for _, it := range h.R.Members {
		if it == nil {
			continue
		}
		if cont := f(it); !cont {
			return
		}
	}
}
