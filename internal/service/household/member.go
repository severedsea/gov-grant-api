package household

import (
	"context"
	"time"

	"github.com/volatiletech/null/v8"
	"github.com/volatiletech/sqlboiler/v4/boil"
	"github.com/volatiletech/sqlboiler/v4/queries/qm"
	"gitlab.com/severedsea/golang-kit/db"
	"gitlab.com/severedsea/golang-kit/web"
	"gitlab.com/severedsea/gov-grant-api/internal/models"
)

// AddMemberInput represents the input struct for adding a member into a household
type AddMemberInput struct {
	HouseholdID       int
	FirstName         string
	LastName          *string
	Sex               MemberSex
	MaritalStatus     MemberMaritalStatus
	SpouseMemberID    *int
	OccupationType    MemberOccupationType
	AnnualIncomeCents int
	DateOfBirth       time.Time
}

// Validate validates the input values
func (i *AddMemberInput) validate(ctx context.Context, tx db.Transactor) error {
	// Validate sex enum
	if !i.Sex.IsValid() {
		return ErrInvalidSex
	}
	// Validate occupation type enum
	if !i.OccupationType.IsValid() {
		return ErrInvalidOccupationType
	}
	// Validate marital status enum
	if !i.MaritalStatus.IsValid() {
		return ErrInvalidMaritalStatus
	}
	// Validate minimum income
	if i.AnnualIncomeCents < 0 {
		return ErrMinimumAnnualIncome
	}
	// Validate household ID
	exists, err := models.HouseholdExists(ctx, tx, i.HouseholdID)
	if err != nil {
		return web.NewError(ErrDatabase, err.Error())
	}
	if !exists {
		return ErrHouseholdNotFound
	}

	// Validate spouse member ID
	if i.SpouseMemberID != nil {
		exists, err := models.MemberExists(ctx, tx, *i.SpouseMemberID)
		if err != nil {
			return web.NewError(ErrDatabase, err.Error())
		}
		if !exists {
			return ErrSpouseMemberNotFound
		}
	}

	return nil
}

// ToModel converts the input to model
func (i *AddMemberInput) toModel() models.Member {
	m := models.Member{
		HouseholdID:       i.HouseholdID,
		FirstName:         i.FirstName,
		LastName:          null.StringFromPtr(i.LastName),
		Sex:               string(i.Sex),
		MaritalStatus:     string(i.MaritalStatus),
		OccupationType:    string(i.OccupationType),
		SpouseMemberID:    null.IntFromPtr(i.SpouseMemberID),
		AnnualIncomeCents: i.AnnualIncomeCents,
		Dob:               i.DateOfBirth,
	}
	return m
}

// AddMember adds a member into a household
func (Service) AddMember(ctx context.Context, tx db.Transactor, input AddMemberInput) (models.Member, error) {
	// Validate input
	if err := input.validate(ctx, tx); err != nil {
		return models.Member{}, err
	}

	// Convert to model
	m := input.toModel()

	// Insert record
	if err := m.Insert(ctx, tx, boil.Infer()); err != nil {
		return models.Member{}, web.NewError(ErrDatabase, err.Error())
	}

	// Update spouse's spouse (bi-directional)
	if m.SpouseMemberID.Valid {
		if _, err := models.Members(
			models.MemberWhere.ID.EQ(m.SpouseMemberID.Int),
		).UpdateAll(ctx, tx, models.M{
			models.MemberColumns.SpouseMemberID: null.IntFrom(m.ID),
		}); err != nil {
			return models.Member{}, web.NewError(ErrDatabase, err.Error())
		}

		// Reload spouse information
		reloaded, err := models.Members(
			qm.Load(models.MemberRels.SpouseMember),
		).One(ctx, tx)
		if err != nil {
			return models.Member{}, web.NewError(ErrDatabase, err.Error())
		}
		m = *reloaded
	}

	return m, nil
}

// DeleteMember deletes a household member
func (Service) DeleteMember(ctx context.Context, tx db.Transactor, id int) error {
	deleted, err := models.Members(
		models.MemberWhere.ID.EQ(id),
	).DeleteAll(ctx, tx)
	if err != nil {
		return web.NewError(ErrDatabase, err.Error())
	}
	if deleted <= 0 {
		return ErrMemberNotFound
	}
	return nil
}
