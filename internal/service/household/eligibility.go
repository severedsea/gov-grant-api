package household

import (
	"context"
	"sort"
	"time"

	"github.com/volatiletech/sqlboiler/v4/queries/qm"
	"gitlab.com/severedsea/golang-kit/db"
	"gitlab.com/severedsea/golang-kit/web"
	"gitlab.com/severedsea/gov-grant-api/internal/models"
)

var (
	// Applicable grants for evaluation
	applicableGrants = []Grant{StudentEncouragementBonus, FamilyTogethernessScheme, ElderBonus, BabySunshineGrant, YoloGSTGrant}
)

// EligibleGrants is the struct representation for a household contain eligible grants
type EligibleGrants struct {
	Household models.Household
	Grants    []string
}

// SearchInput represents the input struct for searching households
type SearchInput struct {
	MemberCount      *int
	TotalIncomeCents *int
	// Add more search parameters here
}

func (i *SearchInput) queryMods() []qm.QueryMod {
	qms := []qm.QueryMod{
		qm.Load(models.HouseholdRels.Members + "." + models.MemberRels.SpouseMember),
		qm.Select(models.TableNames.Households + ".*"),
		qm.InnerJoin(models.TableNames.Members + " as m ON " + models.TableNames.Households + "." + models.HouseholdColumns.ID + " = m." + models.MemberColumns.HouseholdID),
		qm.GroupBy(models.TableNames.Households + "." + models.HouseholdColumns.ID),
		qm.OrderBy(models.TableNames.Households + "." + models.HouseholdColumns.ID),
	}

	if i.MemberCount != nil {
		qms = append(qms, qm.Having("COUNT(m) >= ?", *i.MemberCount))
	}
	if i.TotalIncomeCents != nil {
		qms = append(qms, qm.Having("SUM(m."+models.MemberColumns.AnnualIncomeCents+") > ?", *i.TotalIncomeCents))
	}
	return qms
}

// SearchEligibleGrants retrieves eligible grants based on the search criteria grouped by qualifying households
func (Service) SearchEligibleGrants(ctx context.Context, tx db.Transactor, input SearchInput) ([]EligibleGrants, error) {
	// Retrieve households based on criteria
	h, err := models.Households(input.queryMods()...).All(ctx, tx)
	if err != nil {
		return []EligibleGrants{}, web.NewError(ErrDatabase, err.Error())
	}

	result := []EligibleGrants{}
	for _, it := range h {
		g, err := getEligibleGrantNames(*it, time.Now())
		if err != nil {
			return []EligibleGrants{}, err
		}

		result = append(result, EligibleGrants{
			Household: *it,
			Grants:    g,
		})
	}

	return result, nil
}

// filterEligibleGrantNames executes all the applicable grant rules and returns eligible grant names
func getEligibleGrantNames(h models.Household, now time.Time) ([]string, error) {
	result := []string{}
	for _, it := range applicableGrants {
		ok, err := it.IsEligible(h, now)
		if err != nil {
			return []string{}, err
		}
		if ok {
			result = append(result, it.Name())
		}
	}

	sort.Strings(result)
	return result, nil
}
