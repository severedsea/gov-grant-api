package household

import (
	"context"
	"database/sql"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/severedsea/golang-kit/db"
	"gitlab.com/severedsea/gov-grant-api/internal/models"
	"gitlab.com/severedsea/gov-grant-api/internal/pkg/seed"
)

func TestSearchEligibleGrants(t *testing.T) {
	two := 2
	one50k := 15000000
	oneCent := 1
	testCases := []struct {
		desc     string
		given    SearchInput
		expected []EligibleGrants
	}{
		{
			desc:  "all",
			given: SearchInput{},
			expected: []EligibleGrants{
				{
					Household: models.Household{ID: 9000},
					Grants: []string{
						BabySunshineGrant.Name(),
						StudentEncouragementBonus.Name(),
					},
				},
				{
					Household: models.Household{ID: 9001},
					Grants: []string{
						BabySunshineGrant.Name(),
						FamilyTogethernessScheme.Name(),
					},
				},
				{
					Household: models.Household{ID: 9002},
					Grants: []string{
						ElderBonus.Name(),
						YoloGSTGrant.Name(),
					},
				},
				{
					Household: models.Household{ID: 9003},
					Grants: []string{
						BabySunshineGrant.Name(),
						StudentEncouragementBonus.Name(),
					},
				},
				{
					Household: models.Household{ID: 9004},
					Grants: []string{
						YoloGSTGrant.Name(),
					},
				},
			},
		},
		{
			desc:  "household size: 2",
			given: SearchInput{MemberCount: &two},
			expected: []EligibleGrants{
				{
					Household: models.Household{ID: 9000},
					Grants: []string{
						BabySunshineGrant.Name(),
						StudentEncouragementBonus.Name(),
					},
				},
				{
					Household: models.Household{ID: 9001},
					Grants: []string{
						BabySunshineGrant.Name(),
						FamilyTogethernessScheme.Name(),
					},
				},
			},
		},
		{
			desc:  "household size: 2, total income: 150,000",
			given: SearchInput{MemberCount: &two, TotalIncomeCents: &one50k},
			expected: []EligibleGrants{
				{
					Household: models.Household{ID: 9001},
					Grants: []string{
						BabySunshineGrant.Name(),
						FamilyTogethernessScheme.Name(),
					},
				},
			},
		},
		{
			desc:  "total income: 0.01",
			given: SearchInput{TotalIncomeCents: &oneCent},
			expected: []EligibleGrants{
				{
					Household: models.Household{ID: 9000},
					Grants: []string{
						BabySunshineGrant.Name(),
						StudentEncouragementBonus.Name(),
					},
				},
				{
					Household: models.Household{ID: 9001},
					Grants: []string{
						BabySunshineGrant.Name(),
						FamilyTogethernessScheme.Name(),
					},
				},
				{
					Household: models.Household{ID: 9003},
					Grants: []string{
						BabySunshineGrant.Name(),
						StudentEncouragementBonus.Name(),
					},
				},
			},
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.desc, func(t *testing.T) {
			err := db.Tx(context.Background(), nil, func(tx db.Transactor) error {
				defer tx.Rollback()
				// Given:
				ctx := context.Background()

				// Prep fixtures:
				seed.LoadTestDataFile(t, tx, "testdata/eligibility.sql")

				// When:
				s := New()
				result, err := s.SearchEligibleGrants(ctx, tx, tc.given)

				// Then:
				require.NoError(t, err)
				require.Len(t, result, len(tc.expected))
				for i, it := range result {
					exp := tc.expected[i]
					require.Equal(t, exp.Household.ID, it.Household.ID)
					require.Len(t, it.Grants, len(exp.Grants), "%d should have %d grants", exp.Household.ID, len(exp.Grants))
					require.Equal(t, exp.Grants, it.Grants)
				}

				return nil
			}, 0)
			if err != nil && err != sql.ErrTxDone {
				require.FailNow(t, err.Error())
			}
		})
	}
}
