package household

import (
	"context"
	"database/sql"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"github.com/volatiletech/sqlboiler/v4/boil"
	"gitlab.com/severedsea/golang-kit/db"
	"gitlab.com/severedsea/gov-grant-api/internal/models"
)

func TestRetrieve(t *testing.T) {
	err := db.Tx(context.Background(), nil, func(tx db.Transactor) error {
		defer tx.Rollback()
		// Given:
		ctx := context.Background()
		given := 1111

		// Prep fixtures:
		householdFixture := models.Household{
			ID:   1111,
			Type: string(TypeLanded),
		}
		memberFixture := models.Member{
			ID:                2222,
			HouseholdID:       householdFixture.ID,
			FirstName:         "Member",
			AnnualIncomeCents: 100,
			MaritalStatus:     string(MemberMaritalStatusMarried),
			OccupationType:    string(MemberOccupationTypeStudent),
			Sex:               string(MemberSexM),
			Dob:               time.Date(1980, 2, 1, 0, 0, 0, 0, time.UTC),
		}
		require.NoError(t, householdFixture.Insert(ctx, tx, boil.Infer()))
		require.NoError(t, memberFixture.Insert(ctx, tx, boil.Infer()))

		// When:
		s := New()
		result, err := s.Retrieve(ctx, tx, given)

		// Then:
		require.NoError(t, err)
		require.Equal(t, householdFixture.ID, result.ID)
		require.Equal(t, householdFixture.Type, result.Type)
		members := result.R.Members
		require.Equal(t, 1, len(members))
		member := members[0]
		require.Equal(t, memberFixture.ID, member.ID)
		require.Equal(t, memberFixture.FirstName, member.FirstName)
		require.Equal(t, memberFixture.LastName, member.LastName)
		require.Equal(t, memberFixture.AnnualIncomeCents, member.AnnualIncomeCents)
		require.Equal(t, memberFixture.MaritalStatus, member.MaritalStatus)
		require.Equal(t, memberFixture.OccupationType, member.OccupationType)
		require.Equal(t, memberFixture.Sex, member.Sex)
		require.Equal(t, memberFixture.Dob, member.Dob)

		return nil
	}, 0)
	if err != nil && err != sql.ErrTxDone {
		require.FailNow(t, err.Error())
	}
}
func TestRetrieveAll(t *testing.T) {
	err := db.Tx(context.Background(), nil, func(tx db.Transactor) error {
		defer tx.Rollback()
		// Given:
		ctx := context.Background()

		// Prep fixtures:
		householdFixture1 := models.Household{
			ID:   1111,
			Type: string(TypeLanded),
		}
		householdFixture2 := models.Household{
			ID:   2222,
			Type: string(TypeCondo),
		}
		memberFixture := models.Member{
			HouseholdID:       householdFixture1.ID,
			ID:                2222,
			FirstName:         "Member",
			AnnualIncomeCents: 100,
			MaritalStatus:     string(MemberMaritalStatusMarried),
			OccupationType:    string(MemberOccupationTypeStudent),
			Sex:               string(MemberSexM),
			Dob:               time.Date(1980, 2, 1, 0, 0, 0, 0, time.UTC),
		}
		require.NoError(t, householdFixture1.Insert(ctx, tx, boil.Infer()))
		require.NoError(t, householdFixture2.Insert(ctx, tx, boil.Infer()))
		require.NoError(t, memberFixture.Insert(ctx, tx, boil.Infer()))

		// When:
		s := New()
		result, err := s.RetrieveAll(ctx, tx)

		// Then:
		require.NoError(t, err)
		require.Equal(t, 2, len(result))
		require.Equal(t, householdFixture1.ID, result[0].ID)
		require.Equal(t, householdFixture1.Type, result[0].Type)
		household1Members := result[0].R.Members
		require.Equal(t, 1, len(household1Members))
		member := household1Members[0]
		require.Equal(t, memberFixture.ID, member.ID)
		require.Equal(t, memberFixture.FirstName, member.FirstName)
		require.Equal(t, memberFixture.LastName, member.LastName)
		require.Equal(t, memberFixture.AnnualIncomeCents, member.AnnualIncomeCents)
		require.Equal(t, memberFixture.MaritalStatus, member.MaritalStatus)
		require.Equal(t, memberFixture.OccupationType, member.OccupationType)
		require.Equal(t, memberFixture.Sex, member.Sex)
		require.Equal(t, memberFixture.Dob, member.Dob)
		require.Equal(t, householdFixture2.ID, result[1].ID)
		require.Equal(t, householdFixture2.Type, result[1].Type)

		return nil
	}, 0)
	if err != nil && err != sql.ErrTxDone {
		require.FailNow(t, err.Error())
	}
}
