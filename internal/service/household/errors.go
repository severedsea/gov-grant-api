package household

import (
	"net/http"

	"gitlab.com/severedsea/golang-kit/web"
)

// Errors
var (
	// 4xx
	ErrInvalidType           = &web.Error{Status: http.StatusBadRequest, Code: "invalid_type", Desc: "Invalid household type"}
	ErrInvalidSex            = &web.Error{Status: http.StatusBadRequest, Code: "invalid_sex", Desc: "Invalid sex type"}
	ErrInvalidMaritalStatus  = &web.Error{Status: http.StatusBadRequest, Code: "invalid_marital_status", Desc: "Invalid marital status"}
	ErrInvalidOccupationType = &web.Error{Status: http.StatusBadRequest, Code: "invalid_occupation_type", Desc: "Invalid occupation type"}
	ErrMinimumAnnualIncome   = &web.Error{Status: http.StatusBadRequest, Code: "minimum_annual_income", Desc: "Annual income must be greater than or equal to 0"}
	ErrHouseholdNotFound     = &web.Error{Status: http.StatusNotFound, Code: "household_not_found", Desc: "Household not found"}
	ErrMemberNotFound        = &web.Error{Status: http.StatusNotFound, Code: "member_not_found", Desc: "Member not found"}
	ErrSpouseMemberNotFound  = &web.Error{Status: http.StatusNotFound, Code: "spouse_member_not_found", Desc: "Spouse member not found"}

	// 5xx
	ErrDatabase = &web.Error{Status: http.StatusInternalServerError, Code: "db"}
)
