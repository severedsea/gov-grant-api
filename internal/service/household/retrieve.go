package household

import (
	"context"
	"database/sql"

	"github.com/volatiletech/sqlboiler/v4/queries/qm"
	"gitlab.com/severedsea/golang-kit/db"
	"gitlab.com/severedsea/golang-kit/web"
	"gitlab.com/severedsea/gov-grant-api/internal/models"
)

// Retrieve retrieves a household by ID
func (Service) Retrieve(ctx context.Context, tx db.Transactor, id int) (models.Household, error) {
	m, err := models.Households(
		qm.Load(models.HouseholdRels.Members+"."+models.MemberRels.SpouseMember),
		models.HouseholdWhere.ID.EQ(id),
	).One(ctx, tx)
	if m == nil || err == sql.ErrNoRows {
		return models.Household{}, ErrHouseholdNotFound
	}
	if err != nil {
		return models.Household{}, web.NewError(ErrDatabase, err.Error())
	}
	return *m, nil
}

// RetrieveAll retrieves all households
func (Service) RetrieveAll(ctx context.Context, tx db.Transactor) (models.HouseholdSlice, error) {
	m, err := models.Households(
		qm.Load(models.HouseholdRels.Members+"."+models.MemberRels.SpouseMember),
		qm.OrderBy(models.HouseholdColumns.ID+" ASC"),
	).All(ctx, tx)
	if err != nil {
		return models.HouseholdSlice{}, web.NewError(ErrDatabase, err.Error())
	}
	return m, nil
}
