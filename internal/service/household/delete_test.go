package household

import (
	"context"
	"database/sql"
	"testing"

	"github.com/stretchr/testify/require"
	"github.com/volatiletech/sqlboiler/v4/boil"
	"gitlab.com/severedsea/golang-kit/db"
	"gitlab.com/severedsea/gov-grant-api/internal/models"
)

func TestDelete(t *testing.T) {
	err := db.Tx(context.Background(), nil, func(tx db.Transactor) error {
		defer tx.Rollback()
		// Given:
		ctx := context.Background()

		// Prep fixtures:
		householdFixture := models.Household{
			ID:   1111,
			Type: string(TypeLanded),
		}
		require.NoError(t, householdFixture.Insert(ctx, tx, boil.Infer()))
		memberFixture := models.Member{
			ID:          9999,
			HouseholdID: householdFixture.ID,
		}
		memberFixture.Insert(ctx, tx, boil.Infer())

		// When:
		s := New()
		err := s.Delete(ctx, tx, householdFixture.ID)

		// Then:
		require.NoError(t, err)
		exists, err := models.HouseholdExists(ctx, tx, householdFixture.ID)
		require.NoError(t, err)
		require.False(t, exists)
		exists, err = models.MemberExists(ctx, tx, memberFixture.ID)
		require.NoError(t, err)
		require.False(t, exists)

		return nil
	}, 0)
	if err != nil && err != sql.ErrTxDone {
		require.FailNow(t, err.Error())
	}
}

func TestDelete_Error(t *testing.T) {
	err := db.Tx(context.Background(), nil, func(tx db.Transactor) error {
		defer tx.Rollback()
		// Given:
		ctx := context.Background()

		// Prep fixtures:
		// No fixtures

		// When:
		s := New()
		err := s.Delete(ctx, tx, 9999)

		// Then:
		require.Error(t, err)
		require.Equal(t, ErrHouseholdNotFound, err)

		return nil
	}, 0)
	if err != nil && err != sql.ErrTxDone {
		require.FailNow(t, err.Error())
	}
}
