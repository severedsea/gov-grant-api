package household

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestType_IsValid(t *testing.T) {
	testCases := []struct {
		given    Type
		expected bool
	}{
		{TypeLanded, true},
		{TypeCondo, true},
		{TypeHDB, true},
		{Type("INVALID"), false},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(string(tc.given), func(t *testing.T) {
			t.Parallel()
			// Given:

			// When:
			result := tc.given.IsValid()

			// Then:
			require.Equal(t, tc.expected, result)
		})
	}
}
