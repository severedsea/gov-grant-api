package household

import (
	"context"
	"database/sql"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/severedsea/golang-kit/db"
	"gitlab.com/severedsea/golang-kit/web"
	"gitlab.com/severedsea/gov-grant-api/internal/models"
)

func TestCreate(t *testing.T) {
	testCases := []struct {
		desc     string
		given    CreateInput
		expected models.Household
	}{
		{
			desc:     "landed",
			given:    CreateInput{Type: TypeLanded},
			expected: models.Household{Type: string(TypeLanded)},
		},
		{
			desc:     "condo",
			given:    CreateInput{Type: TypeCondo},
			expected: models.Household{Type: string(TypeCondo)},
		},
		{
			desc:     "hdb",
			given:    CreateInput{Type: TypeHDB},
			expected: models.Household{Type: string(TypeHDB)},
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.desc, func(t *testing.T) {
			err := db.Tx(context.Background(), nil, func(tx db.Transactor) error {
				defer tx.Rollback()

				// Given:
				ctx := context.Background()

				// When:
				s := New()
				result, err := s.Create(ctx, tx, tc.given)

				// Then:
				require.NoError(t, err)
				saved, err := models.FindHousehold(ctx, tx, result.ID)
				require.NoError(t, err)
				require.Equal(t, tc.expected.Type, saved.Type, "type")
				require.False(t, saved.CreatedAt.IsZero())
				require.False(t, saved.UpdatedAt.IsZero())

				return nil
			}, 0)
			if err != nil && err != sql.ErrTxDone {
				require.FailNow(t, err.Error())
			}
		})
	}
}

func TestCreate_Error(t *testing.T) {
	testCases := []struct {
		desc     string
		given    CreateInput
		expected *web.Error
	}{
		{
			desc:     "invalid type",
			given:    CreateInput{Type: Type("INVALID")},
			expected: ErrInvalidType,
		},
	}
	for _, tc := range testCases {
		tc := tc
		t.Run(tc.desc, func(t *testing.T) {
			err := db.Tx(context.Background(), nil, func(tx db.Transactor) error {
				defer tx.Rollback()

				// Given:
				ctx := context.Background()

				// When:
				s := New()
				result, err := s.Create(ctx, tx, tc.given)

				// Then:
				require.Error(t, err)
				require.Empty(t, result)
				require.Equal(t, tc.expected, err)

				return nil
			}, 0)
			if err != nil && err != sql.ErrTxDone {
				require.FailNow(t, err.Error())
			}
		})
	}
}
