package household

// New creates a new Service struct
func New() Service {
	return Service{}
}

// Service holds the methods for this package
type Service struct{}
