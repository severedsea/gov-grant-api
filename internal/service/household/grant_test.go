package household

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"github.com/volatiletech/null/v8"
	"gitlab.com/severedsea/gov-grant-api/internal/models"
)

func TestStudentEncouragementBonus(t *testing.T) {
	now := time.Date(1996, 10, 1, 0, 0, 0, 0, time.UTC)

	testCases := []struct {
		desc          string
		memberAges    []time.Time
		annualIncomes []int
		expected      bool
	}{
		{
			desc: "income: 150000, age: 16 years",
			memberAges: []time.Time{
				now.AddDate(-16, 0, 0),
			},
			annualIncomes: []int{15000000},
			expected:      false,
		},
		{
			desc: "income: 149999.99, age: 16 years",
			memberAges: []time.Time{
				now.AddDate(-16, 0, 0),
			},
			annualIncomes: []int{14999999},
			expected:      false,
		},
		{
			desc: "income: 150000, age: 15 years",
			memberAges: []time.Time{
				now.AddDate(-15, 0, 0),
			},
			annualIncomes: []int{15000000},
			expected:      false,
		},
		{
			desc: "income: 149999.99, age: 15 years",
			memberAges: []time.Time{
				now.AddDate(-15, 0, 0),
			},
			annualIncomes: []int{14999999},
			expected:      true,
		},
		{
			desc: "income: 150000, age: 15,16,20 years",
			memberAges: []time.Time{
				now.AddDate(-15, 0, 0),
				now.AddDate(-16, 0, 0),
				now.AddDate(-20, 0, 0),
			},
			annualIncomes: []int{1000000, 1000000, 13000000},
			expected:      false,
		},
		{
			desc: "income: 149999.99, age: 15,16,20 years",
			memberAges: []time.Time{
				now.AddDate(-15, 0, 0),
				now.AddDate(-16, 0, 0),
				now.AddDate(-20, 0, 0),
			},
			annualIncomes: []int{1000000, 999999, 13000000},
			expected:      true,
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.desc, func(t *testing.T) {
			// Given:
			given := models.Household{
				ID: 1111,
			}
			given.R = given.R.NewStruct()
			given.R.Members = models.MemberSlice{}
			require.Equal(t, len(tc.annualIncomes), len(tc.memberAges))
			for i, it := range tc.memberAges {
				given.R.Members = append(given.R.Members, &models.Member{
					Dob:               it,
					AnnualIncomeCents: tc.annualIncomes[i],
				})
			}

			// When:
			result, err := StudentEncouragementBonus.IsEligible(given, now)

			// Then:
			require.NoError(t, err)
			require.Equal(t, tc.expected, result)
		})
	}
}

func TestFamilyTogethernessScheme(t *testing.T) {
	now := time.Date(1996, 10, 1, 0, 0, 0, 0, time.UTC)
	one := 1
	two := 2
	testCases := []struct {
		desc           string
		memberAges     []time.Time
		membersSpouses map[int]*int
		expected       bool
	}{
		{
			desc: "couple: yes, age: 18 years",
			memberAges: []time.Time{
				now.AddDate(-30, 0, 0),
				now.AddDate(-30, 0, 0),
				now.AddDate(-18, 0, 0),
			},
			membersSpouses: map[int]*int{
				one: &two,
				two: nil,
				3:   nil,
			},
			expected: false,
		},
		{
			desc: "couple: no, age: 17 years",
			memberAges: []time.Time{
				now.AddDate(-30, 0, 0),
				now.AddDate(-17, 0, 0),
			},
			membersSpouses: map[int]*int{
				one: &two,
				3:   nil,
			},
			expected: false,
		},
		{
			desc: "couple: yes, age: 17 years",
			memberAges: []time.Time{
				now.AddDate(-30, 0, 0),
				now.AddDate(-30, 0, 0),
				now.AddDate(-17, 0, 0),
			},
			membersSpouses: map[int]*int{
				one: &two,
				two: nil,
				3:   nil,
			},
			expected: true,
		},
		{
			desc: "couple: no, age: 17,18,20 years",
			memberAges: []time.Time{
				now.AddDate(-30, 0, 0),
				now.AddDate(-17, 0, 0),
				now.AddDate(-18, 0, 0),
				now.AddDate(-20, 0, 0),
			},
			membersSpouses: map[int]*int{
				one: &two,
				3:   nil,
				4:   nil,
				5:   nil,
			},
			expected: false,
		},
		{
			desc: "couple: yes, age: 17,18,20 years",
			memberAges: []time.Time{
				now.AddDate(-30, 0, 0),
				now.AddDate(-30, 0, 0),
				now.AddDate(-17, 0, 0),
				now.AddDate(-16, 0, 0),
				now.AddDate(-20, 0, 0),
			},
			membersSpouses: map[int]*int{
				one: &two,
				two: nil,
				3:   nil,
				4:   nil,
				5:   nil,
			},
			expected: true,
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.desc, func(t *testing.T) {
			// Given:
			given := models.Household{
				ID: 1111,
			}
			given.R = given.R.NewStruct()
			given.R.Members = models.MemberSlice{}
			require.Equal(t, len(tc.membersSpouses), len(tc.memberAges))
			var i int
			for mID, sID := range tc.membersSpouses {
				given.R.Members = append(given.R.Members, &models.Member{
					ID:             mID,
					SpouseMemberID: null.IntFromPtr(sID),
					Dob:            tc.memberAges[i],
				})
				i++
			}

			// When:
			result, err := FamilyTogethernessScheme.IsEligible(given, now)

			// Then:
			require.NoError(t, err)
			require.Equal(t, tc.expected, result)
		})
	}
}

func TestElderBonus(t *testing.T) {
	now := time.Date(1996, 10, 1, 0, 0, 0, 0, time.UTC)
	testCases := []struct {
		desc          string
		householdType Type
		memberAges    []time.Time
		expected      bool
	}{
		{
			desc:          "type: hdb, age: 30,30,50 years",
			householdType: TypeHDB,
			memberAges: []time.Time{
				now.AddDate(-30, 0, 0),
				now.AddDate(-30, 0, 0),
				now.AddDate(-50, 0, 0),
			},
			expected: false,
		},
		{
			desc:          "type: condo, age: 30,30,50 years",
			householdType: TypeCondo,
			memberAges: []time.Time{
				now.AddDate(-30, 0, 0),
				now.AddDate(-30, 0, 0),
				now.AddDate(-50, 0, 0),
			},
			expected: false,
		},
		{
			desc:          "type: hdb, age: 50 years",
			householdType: TypeHDB,
			memberAges: []time.Time{
				now.AddDate(-50, 0, 0),
			},
			expected: false,
		},
		{
			desc:          "type: hdb, age: 49 years",
			householdType: TypeHDB,
			memberAges: []time.Time{
				now.AddDate(-49, 0, 0),
			},
			expected: false,
		},
		{
			desc:          "type: condo, age: 51 years",
			householdType: TypeCondo,
			memberAges: []time.Time{
				now.AddDate(-51, 0, 0),
			},
			expected: false,
		},
		{
			desc:          "type: condo, age: 30,30,51 years",
			householdType: TypeCondo,
			memberAges: []time.Time{
				now.AddDate(-30, 0, 0),
				now.AddDate(-30, 0, 0),
				now.AddDate(-51, 0, 0),
			},
			expected: false,
		},
		{
			desc:          "type: hdb, age: 51 years",
			householdType: TypeHDB,
			memberAges: []time.Time{
				now.AddDate(-51, 0, 0),
			},
			expected: true,
		},
		{
			desc:          "type: hdb, age: 30,30,51 years",
			householdType: TypeHDB,
			memberAges: []time.Time{
				now.AddDate(-30, 0, 0),
				now.AddDate(-30, 0, 0),
				now.AddDate(-51, 0, 0),
			},
			expected: true,
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.desc, func(t *testing.T) {
			// Given:
			given := models.Household{
				ID:   1111,
				Type: string(tc.householdType),
			}
			given.R = given.R.NewStruct()
			given.R.Members = models.MemberSlice{}
			for _, it := range tc.memberAges {
				given.R.Members = append(given.R.Members, &models.Member{
					Dob: it,
				})
			}

			// When:
			result, err := ElderBonus.IsEligible(given, now)

			// Then:
			require.NoError(t, err)
			require.Equal(t, tc.expected, result)
		})
	}
}
func TestBabySunshineGrant(t *testing.T) {
	now := time.Date(1996, 10, 1, 0, 0, 0, 0, time.UTC)
	testCases := []struct {
		desc       string
		memberAges []time.Time
		expected   bool
	}{
		{
			desc: "age: 30,30,5 years",
			memberAges: []time.Time{
				now.AddDate(-30, 0, 0),
				now.AddDate(-30, 0, 0),
				now.AddDate(-5, 0, 0),
			},
			expected: false,
		},
		{
			desc: "age: 5 years",
			memberAges: []time.Time{
				now.AddDate(-5, 0, 0),
			},
			expected: false,
		},
		{
			desc: "age: 6 years",
			memberAges: []time.Time{
				now.AddDate(-6, 0, 0),
			},
			expected: false,
		},
		{
			desc: "age: 30,30,6 years",
			memberAges: []time.Time{
				now.AddDate(-30, 0, 0),
				now.AddDate(-30, 0, 0),
				now.AddDate(-6, 0, 0),
			},
			expected: false,
		},
		{
			desc: "age: 30,30,4 years",
			memberAges: []time.Time{
				now.AddDate(-30, 0, 0),
				now.AddDate(-30, 0, 0),
				now.AddDate(-4, 0, 0),
			},
			expected: true,
		},
		{
			desc: "age: 4 years",
			memberAges: []time.Time{
				now.AddDate(-4, 0, 0),
			},
			expected: true,
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.desc, func(t *testing.T) {
			// Given:
			given := models.Household{
				ID: 1111,
			}
			given.R = given.R.NewStruct()
			given.R.Members = models.MemberSlice{}
			for _, it := range tc.memberAges {
				given.R.Members = append(given.R.Members, &models.Member{
					Dob: it,
				})
			}

			// When:
			result, err := BabySunshineGrant.IsEligible(given, now)

			// Then:
			require.NoError(t, err)
			require.Equal(t, tc.expected, result)
		})
	}
}

func TestYoloGSTGrant(t *testing.T) {
	now := time.Date(1996, 10, 1, 0, 0, 0, 0, time.UTC)

	testCases := []struct {
		desc          string
		householdType Type
		annualIncomes []int
		expected      bool
	}{
		{
			desc:          "income: 100000, type: condo",
			annualIncomes: []int{10000000},
			householdType: TypeCondo,
			expected:      false,
		},
		{
			desc:          "income: 100000, type: hdb",
			annualIncomes: []int{10000000},
			householdType: TypeHDB,
			expected:      false,
		},
		{
			desc:          "income: 99999.99, type: condo",
			annualIncomes: []int{9999999},
			householdType: TypeCondo,
			expected:      false,
		},
		{
			desc:          "income: 99999.99, type: hdb",
			annualIncomes: []int{9999999},
			householdType: TypeHDB,
			expected:      true,
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.desc, func(t *testing.T) {
			// Given:
			given := models.Household{
				ID:   1111,
				Type: string(tc.householdType),
			}
			given.R = given.R.NewStruct()
			given.R.Members = models.MemberSlice{}
			for _, it := range tc.annualIncomes {
				given.R.Members = append(given.R.Members, &models.Member{
					AnnualIncomeCents: it,
				})
			}

			// When:
			result, err := YoloGSTGrant.IsEligible(given, now)

			// Then:
			require.NoError(t, err)
			require.Equal(t, tc.expected, result)
		})
	}
}

func TestHasMemberAgeLessThan(t *testing.T) {
	testCases := []struct {
		desc       string
		now        time.Time
		years      int
		memberAges []time.Time
		expected   bool
	}{
		{
			desc:  "equal",
			now:   time.Date(1996, 10, 1, 0, 0, 0, 0, time.UTC),
			years: 16,
			memberAges: []time.Time{
				time.Date(1980, 10, 1, 0, 0, 0, 0, time.UTC),
			},
			expected: false,
		},
		{
			desc:  "-1d",
			now:   time.Date(1996, 10, 1, 0, 0, 0, 0, time.UTC),
			years: 16,
			memberAges: []time.Time{
				time.Date(1980, 10, 2, 0, 0, 0, 0, time.UTC),
			},
			expected: true,
		},
		{
			desc:  "+1d",
			now:   time.Date(1996, 10, 1, 0, 0, 0, 0, time.UTC),
			years: 16,
			memberAges: []time.Time{
				time.Date(1980, 9, 30, 0, 0, 0, 0, time.UTC),
			},
			expected: false,
		},
		{
			desc:  "equal, +1d",
			now:   time.Date(1996, 10, 1, 0, 0, 0, 0, time.UTC),
			years: 16,
			memberAges: []time.Time{
				time.Date(1980, 9, 30, 0, 0, 0, 0, time.UTC),
				time.Date(1980, 10, 1, 0, 0, 0, 0, time.UTC),
			},
			expected: false,
		},
		{
			desc:  "equal, +1d, -1d",
			now:   time.Date(1996, 10, 1, 0, 0, 0, 0, time.UTC),
			years: 16,
			memberAges: []time.Time{
				time.Date(1980, 9, 30, 0, 0, 0, 0, time.UTC),
				time.Date(1980, 10, 1, 0, 0, 0, 0, time.UTC),
				time.Date(1980, 10, 2, 0, 0, 0, 0, time.UTC),
			},
			expected: true,
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.desc, func(t *testing.T) {
			t.Parallel()
			// Given:
			given := models.Household{
				ID: 1111,
			}
			given.R = given.R.NewStruct()
			given.R.Members = models.MemberSlice{}

			for _, it := range tc.memberAges {
				given.R.Members = append(given.R.Members, &models.Member{
					Dob: it,
				})
			}

			// When:
			result := hasMemberAgeLessThan(given, tc.years, tc.now)

			// Then:
			require.Equal(t, tc.expected, result)
		})
	}
}

func TestHasMemberAgeGreaterThan(t *testing.T) {
	testCases := []struct {
		desc       string
		now        time.Time
		years      int
		memberAges []time.Time
		expected   bool
	}{
		{
			desc:  "equal",
			now:   time.Date(1996, 10, 1, 0, 0, 0, 0, time.UTC),
			years: 16,
			memberAges: []time.Time{
				time.Date(1980, 10, 1, 0, 0, 0, 0, time.UTC),
			},
			expected: false,
		},
		{
			desc:  "-1d",
			now:   time.Date(1996, 10, 1, 0, 0, 0, 0, time.UTC),
			years: 16,
			memberAges: []time.Time{
				time.Date(1980, 10, 2, 0, 0, 0, 0, time.UTC),
			},
			expected: false,
		},
		{
			desc:  "+1d",
			now:   time.Date(1996, 10, 1, 0, 0, 0, 0, time.UTC),
			years: 16,
			memberAges: []time.Time{
				time.Date(1980, 9, 30, 0, 0, 0, 0, time.UTC),
			},
			expected: true,
		},
		{
			desc:  "equal, +1d",
			now:   time.Date(1996, 10, 1, 0, 0, 0, 0, time.UTC),
			years: 16,
			memberAges: []time.Time{
				time.Date(1980, 10, 1, 0, 0, 0, 0, time.UTC),
				time.Date(1980, 10, 2, 0, 0, 0, 0, time.UTC),
			},
			expected: false,
		},
		{
			desc:  "equal, +1d, -1d",
			now:   time.Date(1996, 10, 1, 0, 0, 0, 0, time.UTC),
			years: 16,
			memberAges: []time.Time{
				time.Date(1980, 9, 30, 0, 0, 0, 0, time.UTC),
				time.Date(1980, 10, 1, 0, 0, 0, 0, time.UTC),
				time.Date(1980, 10, 2, 0, 0, 0, 0, time.UTC),
			},
			expected: true,
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.desc, func(t *testing.T) {
			t.Parallel()
			// Given:
			given := models.Household{
				ID: 1111,
			}
			given.R = given.R.NewStruct()
			given.R.Members = models.MemberSlice{}

			for _, it := range tc.memberAges {
				given.R.Members = append(given.R.Members, &models.Member{
					Dob: it,
				})
			}

			// When:
			result := hasMemberAgeGreaterThan(given, tc.years, tc.now)

			// Then:
			require.Equal(t, tc.expected, result)
		})
	}
}

func TestHasCouple(t *testing.T) {
	one := 1
	two := 2
	testCases := []struct {
		desc           string
		membersSpouses map[int]*int
		expected       bool
	}{
		{
			desc: "has couple (uni-directional)",
			membersSpouses: map[int]*int{
				one: &two,
				3:   nil,
				two: nil,
			},
			expected: true,
		},
		{
			desc: "has couple (bi-directional)",
			membersSpouses: map[int]*int{
				one: &two,
				3:   nil,
				two: &one,
			},
			expected: true,
		},
		{
			desc: "spouse not in the household",
			membersSpouses: map[int]*int{
				one: &two,
				3:   nil,
			},
			expected: false,
		},
		{
			desc: "no spouse",
			membersSpouses: map[int]*int{
				one: nil,
				3:   nil,
			},
			expected: false,
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.desc, func(t *testing.T) {
			t.Parallel()
			// Given:
			given := models.Household{
				ID: 1111,
			}
			given.R = given.R.NewStruct()
			given.R.Members = models.MemberSlice{}

			for mID, sID := range tc.membersSpouses {
				given.R.Members = append(given.R.Members, &models.Member{
					ID:             mID,
					SpouseMemberID: null.IntFromPtr(sID),
				})
			}

			// When:
			result := hasCouple(given)

			// Then:
			require.Equal(t, tc.expected, result)
		})
	}
}
