package household

import (
	"context"
	"database/sql"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"github.com/volatiletech/null/v8"
	"github.com/volatiletech/sqlboiler/v4/boil"
	"github.com/volatiletech/sqlboiler/v4/queries/qm"
	"gitlab.com/severedsea/golang-kit/db"
	"gitlab.com/severedsea/golang-kit/web"
	"gitlab.com/severedsea/gov-grant-api/internal/models"
)

func TestAddMember(t *testing.T) {
	householdFixture := models.Household{
		ID:   1111,
		Type: string(TypeLanded),
	}
	spouseFixture := models.Member{
		HouseholdID:       householdFixture.ID,
		ID:                2222,
		FirstName:         "Spouse",
		AnnualIncomeCents: 10000000,
		MaritalStatus:     string(MemberMaritalStatusMarried),
		OccupationType:    string(MemberOccupationTypeEmployed),
		Sex:               string(MemberSexF),
		Dob:               time.Date(1980, 1, 1, 0, 0, 0, 0, time.UTC),
	}
	testCases := []struct {
		desc     string
		given    AddMemberInput
		expected models.Member
	}{
		{
			desc: "with spouse",
			given: AddMemberInput{
				HouseholdID:       householdFixture.ID,
				FirstName:         "Member",
				AnnualIncomeCents: 100,
				MaritalStatus:     MemberMaritalStatusMarried,
				SpouseMemberID:    &spouseFixture.ID,
				OccupationType:    MemberOccupationTypeStudent,
				Sex:               MemberSexM,
				DateOfBirth:       time.Date(1980, 2, 1, 0, 0, 0, 0, time.UTC),
			},
			expected: models.Member{
				HouseholdID:       householdFixture.ID,
				FirstName:         "Member",
				AnnualIncomeCents: 100,
				MaritalStatus:     string(MemberMaritalStatusMarried),
				SpouseMemberID:    null.IntFrom(spouseFixture.ID),
				OccupationType:    string(MemberOccupationTypeStudent),
				Sex:               string(MemberSexM),
				Dob:               time.Date(1980, 2, 1, 0, 0, 0, 0, time.UTC),
			},
		},
		{
			desc: "no spouse",
			given: AddMemberInput{
				HouseholdID:       householdFixture.ID,
				FirstName:         "Member",
				AnnualIncomeCents: 100,
				MaritalStatus:     MemberMaritalStatusMarried,
				SpouseMemberID:    nil,
				OccupationType:    MemberOccupationTypeStudent,
				Sex:               MemberSexM,
				DateOfBirth:       time.Date(1980, 2, 1, 0, 0, 0, 0, time.UTC),
			},
			expected: models.Member{
				HouseholdID:       householdFixture.ID,
				FirstName:         "Member",
				AnnualIncomeCents: 100,
				MaritalStatus:     string(MemberMaritalStatusMarried),
				OccupationType:    string(MemberOccupationTypeStudent),
				Sex:               string(MemberSexM),
				Dob:               time.Date(1980, 2, 1, 0, 0, 0, 0, time.UTC),
			},
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.desc, func(t *testing.T) {
			err := db.Tx(context.Background(), nil, func(tx db.Transactor) error {
				defer tx.Rollback()

				// Given:
				ctx := context.Background()

				// Prep fixtures:
				require.NoError(t, householdFixture.Insert(ctx, tx, boil.Infer()))
				require.NoError(t, spouseFixture.Insert(ctx, tx, boil.Infer()))

				// When:
				s := New()
				result, err := s.AddMember(ctx, tx, tc.given)

				// Then:
				require.NoError(t, err)
				saved, err := models.Members(
					qm.Load(models.MemberRels.Household),
					models.MemberWhere.ID.EQ(result.ID),
				).One(ctx, tx)
				require.NoError(t, err)
				require.Equal(t, tc.expected.FirstName, saved.FirstName)
				require.Equal(t, tc.expected.HouseholdID, saved.HouseholdID)
				require.Equal(t, tc.expected.LastName, saved.LastName)
				require.Equal(t, tc.expected.AnnualIncomeCents, saved.AnnualIncomeCents)
				require.Equal(t, tc.expected.MaritalStatus, saved.MaritalStatus)
				require.Equal(t, tc.expected.Sex, saved.Sex)
				require.Equal(t, tc.expected.OccupationType, saved.OccupationType)
				require.Equal(t, tc.expected.Dob.Unix(), saved.Dob.Unix())
				require.Equal(t, tc.expected.SpouseMemberID, saved.SpouseMemberID)
				if tc.expected.SpouseMemberID.Valid {
					require.NotNil(t, result.R)
					require.NotNil(t, result.R.SpouseMember)
					require.Equal(t, tc.expected.SpouseMemberID.Int, result.R.SpouseMember.ID)
				}
				require.False(t, saved.CreatedAt.IsZero(), "has created_at")
				require.False(t, saved.UpdatedAt.IsZero(), "has updated_at")
				// Assert added to household
				require.NotNil(t, saved.R.Household, "added to household")

				return nil
			}, 0)
			if err != nil && err != sql.ErrTxDone {
				require.FailNow(t, err.Error())
			}
		})
	}
}

func TestAddMember_Error(t *testing.T) {
	householdFixture := models.Household{
		ID:   1111,
		Type: string(TypeLanded),
	}
	spouseFixture := models.Member{
		ID:                2222,
		FirstName:         "Spouse",
		AnnualIncomeCents: 10000000,
		MaritalStatus:     string(MemberMaritalStatusMarried),
		OccupationType:    string(MemberOccupationTypeEmployed),
		Sex:               string(MemberSexF),
		Dob:               time.Date(1980, 1, 1, 0, 0, 0, 0, time.UTC),
	}
	invalidID := 99999
	testCases := []struct {
		desc     string
		given    AddMemberInput
		expected *web.Error
	}{
		{
			desc: "household not found",
			given: AddMemberInput{
				HouseholdID:       invalidID,
				FirstName:         "Member",
				AnnualIncomeCents: 100,
				MaritalStatus:     MemberMaritalStatusMarried,
				SpouseMemberID:    nil,
				OccupationType:    MemberOccupationTypeStudent,
				Sex:               MemberSexM,
				DateOfBirth:       time.Date(1980, 2, 1, 0, 0, 0, 0, time.UTC),
			},
			expected: ErrHouseholdNotFound,
		},
		{
			desc: "minimum annual income",
			given: AddMemberInput{
				HouseholdID:       householdFixture.ID,
				FirstName:         "Member",
				AnnualIncomeCents: -1,
				MaritalStatus:     MemberMaritalStatusMarried,
				SpouseMemberID:    nil,
				OccupationType:    MemberOccupationTypeStudent,
				Sex:               MemberSexM,
				DateOfBirth:       time.Date(1980, 2, 1, 0, 0, 0, 0, time.UTC),
			},
			expected: ErrMinimumAnnualIncome,
		},
		{
			desc: "invalid marital status",
			given: AddMemberInput{
				HouseholdID:       householdFixture.ID,
				FirstName:         "Member",
				AnnualIncomeCents: 0,
				MaritalStatus:     MemberMaritalStatus("INVALID"),
				SpouseMemberID:    nil,
				OccupationType:    MemberOccupationTypeStudent,
				Sex:               MemberSexM,
				DateOfBirth:       time.Date(1980, 2, 1, 0, 0, 0, 0, time.UTC),
			},
			expected: ErrInvalidMaritalStatus,
		},
		{
			desc: "invalid occupation type",
			given: AddMemberInput{
				HouseholdID:       householdFixture.ID,
				FirstName:         "Member",
				AnnualIncomeCents: 0,
				MaritalStatus:     MemberMaritalStatusMarried,
				SpouseMemberID:    nil,
				OccupationType:    MemberOccupationType("INVALID"),
				Sex:               MemberSexM,
				DateOfBirth:       time.Date(1980, 2, 1, 0, 0, 0, 0, time.UTC),
			},
			expected: ErrInvalidOccupationType,
		},
		{
			desc: "invalid sex",
			given: AddMemberInput{
				HouseholdID:       householdFixture.ID,
				FirstName:         "Member",
				AnnualIncomeCents: 0,
				MaritalStatus:     MemberMaritalStatusMarried,
				SpouseMemberID:    nil,
				OccupationType:    MemberOccupationTypeStudent,
				Sex:               MemberSex("INVALID"),
				DateOfBirth:       time.Date(1980, 2, 1, 0, 0, 0, 0, time.UTC),
			},
			expected: ErrInvalidSex,
		},
		{
			desc: "invalid spouse member ID",
			given: AddMemberInput{
				HouseholdID:       householdFixture.ID,
				FirstName:         "Member",
				AnnualIncomeCents: 0,
				MaritalStatus:     MemberMaritalStatusMarried,
				SpouseMemberID:    &invalidID,
				OccupationType:    MemberOccupationTypeStudent,
				Sex:               MemberSexM,
				DateOfBirth:       time.Date(1980, 2, 1, 0, 0, 0, 0, time.UTC),
			},
			expected: ErrSpouseMemberNotFound,
		},
	}
	for _, tc := range testCases {
		tc := tc
		t.Run(tc.desc, func(t *testing.T) {
			err := db.Tx(context.Background(), nil, func(tx db.Transactor) error {
				defer tx.Rollback()
				// Given:
				ctx := context.Background()

				// Prep fixtures:
				if tc.given.HouseholdID == householdFixture.ID {
					require.NoError(t, householdFixture.Insert(ctx, tx, boil.Infer()))
				}
				if tc.given.SpouseMemberID != nil && *tc.given.SpouseMemberID == spouseFixture.ID {
					require.NoError(t, spouseFixture.Insert(ctx, tx, boil.Infer()))
				}

				// When:
				s := New()
				result, err := s.AddMember(ctx, tx, tc.given)

				// Then:
				require.Error(t, err)
				require.Empty(t, result)
				require.Equal(t, tc.expected, err)

				return nil
			}, 0)
			if err != nil && err != sql.ErrTxDone {
				require.FailNow(t, err.Error())
			}
		})
	}
}

func TestDeleteMember(t *testing.T) {
	err := db.Tx(context.Background(), nil, func(tx db.Transactor) error {
		defer tx.Rollback()
		// Given:
		ctx := context.Background()

		// Prep fixtures:
		householdFixture := models.Household{
			ID:   1111,
			Type: string(TypeLanded),
		}
		require.NoError(t, householdFixture.Insert(ctx, tx, boil.Infer()))
		memberFixture := models.Member{
			ID:          9999,
			HouseholdID: householdFixture.ID,
		}
		memberFixture.Insert(ctx, tx, boil.Infer())

		// When:
		s := New()
		err := s.DeleteMember(ctx, tx, memberFixture.ID)

		// Then:
		require.NoError(t, err)
		exists, err := models.MemberExists(ctx, tx, memberFixture.ID)
		require.NoError(t, err)
		require.False(t, exists)

		return nil
	}, 0)
	if err != nil && err != sql.ErrTxDone {
		require.FailNow(t, err.Error())
	}
}

func TestDeleteMember_Error(t *testing.T) {
	err := db.Tx(context.Background(), nil, func(tx db.Transactor) error {
		defer tx.Rollback()
		// Given:
		ctx := context.Background()

		// Prep fixtures:
		// No fixtures

		// When:
		s := New()
		err := s.DeleteMember(ctx, tx, 9999)

		// Then:
		require.Error(t, err)
		require.Equal(t, ErrMemberNotFound, err)

		return nil
	}, 0)
	if err != nil && err != sql.ErrTxDone {
		require.FailNow(t, err.Error())
	}
}
