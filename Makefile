.PHONY: test build run db mq redis setup migrate

# APP_NAME is used as a naming convention for resources to the local environment
ifndef APP_NAME
APP_NAME := gov-grant-api
endif
# APP_PATH is the project/app directory in the container
ifndef APP_PATH
APP_PATH := /${APP_NAME}
endif

# ----------------------------
# ----------------------------
# Commands
# ----------------------------
# ----------------------------
COMPOSE := docker-compose -f docker-compose.yml
GO_COMPOSE = $(COMPOSE) run --rm -w $(APP_PATH) -v $(shell pwd):$(APP_PATH) go
RUN_COMPOSE = $(COMPOSE) run --rm --service-ports -w $(APP_PATH) -v $(shell pwd):$(APP_PATH) go

# ----------------------------
# ----------------------------
# Targets
# ----------------------------
# ----------------------------

# ----------------------------
# Setup and Teardown
# ----------------------------
# setup creates/initializes development environment dependencies
setup: db migrate

# db runs the db service defined in the compose file
db:
	$(COMPOSE) up -d db

migrate:
	$(COMPOSE) run --rm -v $(shell pwd)/data/migrations:/migrations migrate \
	sh -c './migrate -path /migrations -database $$DATABASE_URL up'

# dbdrop executes db migration DROP scripts on configured database (i.e. DATABASE_URL)
dbdrop:
	$(COMPOSE) run --rm -v $(shell pwd)/data/migrations:/migrations migrate \
	sh -c './migrate -path /migrations -database $$DATABASE_URL drop'

# compose-down stops and removes all containers and resources associated to docker-compose.yml
compose-down: 
	$(COMPOSE) down

# dbredo drops and re-migrates the database
dbredo: dbdrop migrate

# ----------------------------
# Run
# ----------------------------
# run starts the api server in a golang container
run: setup
	@$(RUN_COMPOSE) env $(shell cat .env | egrep -v '^#|^DATABASE_URL' | xargs) \
		go run cmd/serverd/main.go

# ----------------------------
# Test
# ----------------------------
# test executes project tests in a golang container
NO_COLOR=\033[0m
OK_COLOR=\033[32;01m
ERROR_COLOR=\033[31;01m
test: setup
	@if $(GO_COMPOSE) env $(shell cat .env.dev | egrep -v '^#|^DATABASE_URL' | xargs) \
		make go-test; \
	then printf "\n\n$(OK_COLOR)[Test okay -- `date`]$(NO_COLOR)\n"; \
	else printf "\n\n$(ERROR_COLOR)[Test FAILED -- `date`]$(NO_COLOR)\n"; exit 1; fi

# go-test executes test for all packages
go-test:
	go test -coverprofile=c.out -failfast -timeout 5m ./...

# ----------------------------
# Build
# ----------------------------
# build executes build in a golang container
build: setup
	$(GO_COMPOSE) make go-build

# go-build executes the go build process
go-build:
	go build -v ./cmd/serverd

# ----------------------------
# Tools
# ----------------------------
# update-vendor updates the vendor folder
update-vendor:
	go mod vendor
	go mod tidy

# ----------------------------
# Generate DB boilerplate
# ----------------------------
# boilerplate generates DB boilerplate code 
boilerplate: db migrate generate-models

generate-models: 
	sqlboiler --output internal/models --pkgname models psql
	GOFLAGS="-mod=vendor" goimports -w internal/models/*.go

# generates all gql schemas
generate-graph: generate-graph-public 

# Reads gqlgen.yml and generate graphql models based on defined schema
generate-graph-public: PACKAGE_NAME = public

generate-graph-public: OUTPUT_DIR = cmd/serverd/router/graph/${PACKAGE_NAME}
generate-graph-public:
	## Execute gqlgen for graphql schema
	go run tools/gqlgen/main.go -c $(OUTPUT_DIR)/gqlgen.yml