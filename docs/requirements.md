# Requirements

## Technical Requirements

1.  You may also use a programming framework.
1.  All endpoints should use URL path and HTTP verbs that are appropriate for the action taken.
1.  You do not need to implement any authentication.
1.  You may use a persistent database store to save the data (eg. MySQL, Postgres, Redis, MongoDB, etc). Include instructions on how to setup the database locally (eg. DB schema, SQL scripts to run, command line instructions to migrate the DB).

## Endpoints

### Create Household

This endpoint lets you create the household (housing unit)

Household fields:

- HousingType (Possible options: Landed, Condominium, HDB)

### Add a family member to household

This endpoint lets you add a family member to the household

Family member detail fields:

- Name
- Gender
- MaritalStatus
- Spouse (either name of spouse or primary key)
- OccupationType (Options: Unemployed, Student, Employed)
- AnnualIncome
- DOB

### List households

This endpoint lists all the households in the database

Fields:

- HouseholdType
- FamilyMembers
- Name
- Gender
- MaritalStatus
- Spouse (either name of spouse or primary key)
- OccupationType (Options: Unemployed, Student, Employed)
- AnnualIncome
- DOB

### Show household

This endpoint shows the details of a household in the database

Fields:

- HouseholdType
- FamilyMembers
- Name
- Gender
- MaritalStatus
- OccupationType (Options: Unemployed, Student, Employed)
- AnnualIncome
- DOB

### Search for households and recipients of grant disbursement endpoint.

This endpoint should accept search parameters (eg. Household size, total income) in the URL and return results based on the criteria below.

You may include assumptions about each result in the README.

Grant Schemes:

- List households and qualifying family members for Student Encouragement Bonus
  - Households with children of less than 16 years old.
  - Household income of less than \$150,000.
- List households and qualifying family members for Family Togetherness Scheme
  - Households with husband & wife
  - Has child(ren) younger than 18 years old.
- List households and qualifying family members for Elder Bonus
  - HDB household with family members above the age of 50.
- List households and qualifying family members for Baby Sunshine Grant
  - Household with young children younger than 5.
- List households that qualify for the YOLO GST Grant
  - HDB households with annual income of less than \$100,000.

Disclaimer: All grants mentioned here are fictitious and do not reflect actual grants that are being worked on or implemented by any government ministries.

## Optional Endpoints (for bonus points)

### Delete household

Remove Household and family members.

### Delete Family Member

Remove Family Member from the Household.
