# Design Decisions and Assumptions

- Designed relationship between household and members as one-to-many as it seems logical for an individual to be registered to a single household at any given point in time.
- The operation for creating a member and adding into a household is combined mainly for convenience. It seems the significance of members in this application is to check grant eligibility for a household rather than members being a "user" within the system (since there's no unique identifier for the member such as NRIC). However, in the event it needs to be enhanced, adding a separate resolver/endpoint for managing (CRUD) members should be possible.
- Designed the household grant resolver to return eligible grants grouped by household instead of the other way around (i.e. households grouped by grants) since the search parameters are relative to the household rather than the grants. Adding a new resolver that retrieves all households by grants should be possible if necessary.
- Persisted and handled annual income in cents to avoid floating-point precision issues.
- Used application enum rather than database enumerated types since I personally think it's easier to maintain the application if the logic is in 1 place (application), rather than split between application and database. (i.e. every time we have to add a new type, we have to add and handle on both application and database)
- Structured grants business logic into "rule-like" approach for testability and maintainability
