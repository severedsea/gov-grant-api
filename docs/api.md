# API Documentation

## GraphQL

You can access the GraphQL [schema](../cmd/serverd/router/graph/public/schema), documentation, and playground via: `/graphiql` (i.e. http://localhost:3000/graphiql)

The playground is [meant](../cmd/serverd/router/graph/public/routes) to be enabled only on non-PROD environments.

![Playground](playground.png)

## RESTful

The [requirements](requirements.md) mentioned designing the API using RESTful API style.

Even if the implementation is using GraphQL, below is the API specification in case it needs to be implemented in RESTful API style. Added [comments](../cmd/serverd/router/api/v1/routes.go) as TODO.

### Create household

```
POST /v1/households
```

Request:

```json
{
  "type": "HDB"
}
```

Response:

`200 OK`

```json
{
  "id": 1,
  "type": "HDB"
}
```

### Add member to household

```
POST /v1/households/:id/members
```

Request:

```json
{
  "first_name": "John",
  "last_name": "Smith",
  "sex": "M",
  "marital_status": "MARRIED",
  "spouse_member_id": 11,
  "occupation_type": "EMPLOYED",
  "annual_income_cents": 10000000,
  "date_of_birth": "1980-01-02T00:00:00+08:00"
}
```

Response:

`200 OK`

```json
{
  "id": 1,
  "household_id": 1,
  "first_name": "John",
  "last_name": "Smith",
  "sex": "M",
  "marital_status": "MARRIED",
  "spouse_member_id": 11,
  "occupation_type": "EMPLOYED",
  "annual_income_cents": 10000000,
  "date_of_birth": "1980-01-02T00:00:00+08:00"
}
```

### List households

```
GET /v1/households
```

Response:

`200 OK`

```json
[
  {
    "id": 1,
    "type": "HDB",
    "members": [
      {
        "id": 1,
        "household_id": 1,
        "first_name": "John",
        "last_name": "Smith",
        "sex": "M",
        "marital_status": "MARRIED",
        "spouse_member_id": 11,
        "occupation_type": "EMPLOYED",
        "annual_income_cents": 10000000,
        "date_of_birth": "1980-01-02T00:00:00+08:00:"
      }
    ]
  }
]
```

### Show household

```
GET /v1/households/:id
```

Response:

`200 OK`

```json
{
  "id": 1,
  "type": "HDB",
  "members": [
    {
      "id": 1,
      "household_id": 1,
      "first_name": "John",
      "last_name": "Smith",
      "sex": "M",
      "marital_status": "MARRIED",
      "spouse_member_id": 11,
      "occupation_type": "EMPLOYED",
      "annual_income_cents": 10000000,
      "date_of_birth": "1980-01-02T00:00:00+08:00:"
    }
  ]
}
```

### Search for households and recipients of grant disbursement

```
GET /v1/households?size=1&total_income_cents=1000000
```

Response:

`200 OK`

```json
[
  {
    "id": 1,
    "type": "HDB",
    "members": [
      {
        "id": 1,
        "household_id": 1,
        "first_name": "John",
        "last_name": "Smith",
        "sex": "M",
        "marital_status": "MARRIED",
        "spouse_member_id": 11,
        "occupation_type": "EMPLOYED",
        "annual_income_cents": 10000000,
        "date_of_birth": "1980-01-02T00:00:00+08:00:"
      }
    ],
    "grants": ["Student Encouragement Bonus"]
  }
]
```

### Delete household

```
DELETE /v1/households/:id
```

Response:

`200 OK`

### Delete household member

```
DELETE /v1/members/:id
```

Response:

`200 OK`
