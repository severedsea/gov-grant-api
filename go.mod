module gitlab.com/severedsea/gov-grant-api

go 1.14

require (
	github.com/99designs/gqlgen v0.9.3
	github.com/friendsofgo/errors v0.9.2
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-chi/cors v1.1.1
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.6.1
	github.com/vektah/gqlparser v1.3.1
	github.com/volatiletech/null/v8 v8.1.0
	github.com/volatiletech/sqlboiler/v4 v4.2.0
	github.com/volatiletech/strmangle v0.0.1
	gitlab.com/severedsea/golang-kit v0.0.0-20200725041509-06e560017e84
)
