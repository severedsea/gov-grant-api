package web

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"

	"github.com/pkg/errors"
	"gitlab.com/severedsea/golang-kit/logr"
)

// ParseJSONBody parses JSON request body and handles errors
func ParseJSONBody(result interface{}, r io.ReadCloser) ([]byte, *Error) {
	return ParseAndLogJSONBody(nil, result, r)
}

// ParseAndLogJSONBody parses JSON request body and handles errors
func ParseAndLogJSONBody(logger logr.Logger, result interface{}, r io.ReadCloser) ([]byte, *Error) {
	reqBytes, err := ioutil.ReadAll(r)
	defer r.Close()
	if err != nil {
		return nil, &Error{Status: http.StatusBadRequest, Code: "read_body", Desc: err.Error(), Err: errors.WithStack(err)}
	}

	if logger != nil {
		logger.Infof("JSON Request: %s", string(reqBytes))
	}

	err = json.Unmarshal(reqBytes, &result)
	if err != nil {
		return nil, &Error{Status: http.StatusBadRequest, Code: "parse_body", Desc: err.Error(), Err: errors.WithStack(err)}
	}
	return reqBytes, nil
}
