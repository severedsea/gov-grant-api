package gqlgen

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	"github.com/99designs/gqlgen/graphql"
	"github.com/99designs/gqlgen/handler"
	"github.com/vektah/gqlparser/gqlerror"
	"gitlab.com/severedsea/golang-kit/logr"
	"gitlab.com/severedsea/golang-kit/web"
)

// WebErrorPresenter returns an ErrorPresenter (handler.Option) for web.Error
func WebErrorPresenter() handler.Option {
	return handler.ErrorPresenter(webErrorPresenterFunc)
}

func webErrorPresenterFunc(ctx context.Context, e error) *gqlerror.Error {
	logger := logr.GetLogger(ctx)
	var resolver string

	if resctx := graphql.GetResolverContext(ctx); isRootResolver(resctx) {
		// Add resolver name into logger fields
		name := resctx.Path()[0]
		op := strings.ToLower(resctx.Object)
		if op == "" {
			// Default operation type to query
			op = "query"
		}
		resolver = fmt.Sprintf("%s/%s", op, name)
	}

	err, ok := e.(*web.Error)
	if !ok {
		err = web.NewError(e, e.Error())
	}

	logger.Errorf("Encountered error %d: %s: %s - %s", err.Status, err.Code, err.Desc, resolver)

	// 5XX (except 503) should be sanitized before showing to human
	if err.Status >= 500 && err.Status != http.StatusServiceUnavailable {
		err.Desc = web.GenericErrorMessage
	}

	e = &gqlerror.Error{
		Message: err.Error(),
		Extensions: map[string]interface{}{
			"status":            err.Status,
			"error":             err.Code,
			"error_description": err.Desc,
		},
	}

	return graphql.DefaultErrorPresenter(ctx, e)
}
