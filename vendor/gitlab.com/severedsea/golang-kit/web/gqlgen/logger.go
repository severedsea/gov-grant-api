package gqlgen

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/99designs/gqlgen/graphql"
	"github.com/99designs/gqlgen/handler"
	"gitlab.com/severedsea/golang-kit/logr"
)

// RequestLoggerMiddleware returns a gqlgen request middleware with standard logging implementation
func RequestLoggerMiddleware() handler.Option {
	return handler.RequestMiddleware(func(ctx context.Context, next func(ctx context.Context) []byte) []byte {
		return requestLoggerMiddleware(ctx, next)
	})
}

func requestLoggerMiddleware(ctx context.Context, next func(ctx context.Context) []byte) []byte {
	rctx := graphql.GetRequestContext(ctx)
	if isLoggingDisabled(rctx) {
		return next(ctx)
	}

	logger := logr.GetLogger(ctx)
	var redactedVars string
	rvbytes, err := json.Marshal(rctx.Variables)
	if err != nil {
		redactedVars = "{json marshal failed}"
	} else {
		redactedVars = fmt.Sprintf("%s", rvbytes)
	}
	logger.Infof("[req] q: %s v: %s", rctx.RawQuery, redactedVars)

	resp := next(ctx)

	logger.Infof("Wrote %d bytes: %s", len(resp), resp)

	return resp
}

// ResolverLoggerMiddleware returns a gqlgen resolver middleware with standard logging implementation on root resolvers
func ResolverLoggerMiddleware() handler.Option {
	return handler.ResolverMiddleware(resolverLoggerMiddleware)
}

func resolverLoggerMiddleware(ctx context.Context, next graphql.Resolver) (res interface{}, err error) {
	rctx := graphql.GetRequestContext(ctx)
	if isLoggingDisabled(rctx) {
		return next(ctx)
	}
	if resctx := graphql.GetResolverContext(ctx); isRootResolver(resctx) {
		logger := logr.GetLogger(ctx)

		// Add resolver name into logger fields
		name := resctx.Path()[0]
		op := strings.ToLower(resctx.Object)
		if op == "" {
			// Default operation type to query
			op = "query"
		}
		logger = logger.WithField("resolver", fmt.Sprintf("%s/%s", op, name))
		logger.Debugf("Resolving operation type: %s and root resolver name: %s", resctx.Object, name)

		// Set updated logger to context
		ctx = logr.SetLogger(ctx, logger)
	}

	return next(ctx)
}

// isLoggingDisabled returns true if logging is disabled based on the request context
func isLoggingDisabled(rctx *graphql.RequestContext) bool {
	return isIntrospectionQuery(rctx)
}
