package gqlgen

import (
	"strings"

	"github.com/99designs/gqlgen/graphql"
)

// isIntrospectionQuery returns true if the request is an introspection query
func isIntrospectionQuery(rctx *graphql.RequestContext) bool {
	if rctx == nil {
		return true
	}
	query := rctx.RawQuery
	if strings.Contains(query, "IntrospectionQuery") {
		return true
	}
	return false
}

// isRootResolver returns true the resolver being evaluated is a root method resolver
func isRootResolver(resctx *graphql.ResolverContext) bool {
	return resctx != nil && // Has resolver context
		resctx.IsMethod && // Method resolver
		resctx.Parent != nil && (len(resctx.Parent.Path()) <= 0) && // Root resolver
		len(resctx.Path()) > 0
}
