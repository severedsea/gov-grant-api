// Package gqlgen contains helpers and other SPD-flavoured configuration for https://github.com/99designs/gqlgen such as:
// - Error Presenter
// - Logger Middlewares
//
// ## Error Presenter
// Our kit library has a web package that handles web-related errors using the custom web.Error implementation. In order for
// gqlgen to work with web package, this package provides a gqlgen/handler.ErrorPresenter implementation for web.Error
//
// Example Usage:
//
// // Register graphql route
// r.Handle("/graphql", handler.GraphQL(
//   // add generated schema from gqlgen
//   gqlgen.WebErrorPresenter(),
// ))
//
// ## Logger Middlewares
// The web package has logger middlewares to provide standard for of logging application messages. (i.e. request, response)
// This package provides similar middlewares for gqlgen-powered routes
//
// Example Usage:
//
// // Register graphql route
// r.Handle("/graphql", handler.GraphQL(
//   // add generated schema from gqlgen
//   gqlgen.RequestLoggerMiddleware(),
//   gqlgen.ResolverLoggerMiddleware(),
// ))
package gqlgen
