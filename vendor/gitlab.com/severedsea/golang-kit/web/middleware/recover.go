package middleware

import (
	"errors"
	"net/http"

	"gitlab.com/severedsea/golang-kit/web"
)

// Recover serves as a middleware that recovers from a Recover and sends the panic to sentry
func Recover() Adapter {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			ctx := r.Context()
			defer func() {
				if r := recover(); r != nil {
					err, ok := r.(error)
					if !ok {
						err = errors.New("Unexpected error")
					}

					werr := &web.Error{Status: http.StatusInternalServerError, Code: "unexpected_error", Desc: err.Error(), Err: err}
					web.RespondJSON(ctx, w, werr, nil)
				}
			}()

			next.ServeHTTP(w, r)
		}
		return http.HandlerFunc(fn)
	}
}
