package web

import (
	"net/http"
)

// HandlerFunc is a http.HandlerFunc variant that returns error
type HandlerFunc func(w http.ResponseWriter, r *http.Request) error

// Handler is a http.Handler implementation that handles HandlerFunc
type Handler struct {
	H HandlerFunc
}

func (h Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if err := h.H(w, r); err != nil {
		ctx := r.Context()
		RespondJSON(ctx, w, err, nil)
	}
}
