package db

import (
	"context"
	"time"

	"github.com/cenkalti/backoff"
)

// Tx is a wrapper that provides the callback func with a tx.
//
// Handles errors with backoff retry logic:
// https://docs.microsoft.com/en-us/azure/postgresql/concepts-connectivity#handling-transient-errors
//
// Setting max retries to <= 0 indicates no retry.
func Tx(ctx context.Context, oldTx Transactor, callback func(Transactor) error, maxRetries int) error {
	var b backoff.BackOff
	if maxRetries > 0 {
		b = backoff.WithContext(DefaultBackOff(uint64(maxRetries)), ctx)
	}
	return Transaction(ctx, nil, oldTx, callback, b)
}

// Transaction is a wrapper that provides the callback func with a tx.
// Uses the backoff logic in the argument to retry on opening connection or start tx errors
//
// Sample usage:
// ```
// err := db.Transaction(ctx, nil, nil, func(tx db.Transactor) error {
//   // Do something
//   return nil
// }, db.DefaultBackOff(3))
// ```
//
// Alternatively, you can use the short form db.Tx which uses db.DefaultBackOff internally.
//
// Sample usage:
// ```
// err := db.Tx(ctx, nil, func(tx db.Transactor) error {
//   // Do something
//   return nil
// }, 3)
// ```
func Transaction(ctx context.Context, conn BeginnerExecutor, oldTx Transactor, callback func(Transactor) error, b backoff.BackOff) error {
	// Reuse tx from arguments
	if oldTx != nil {
		return callback(oldTx)
	}

	var tx Transactor
	begin := func() error {
		// If no conneciton provided,
		// use default conn factory
		var err error
		if conn == nil {
			// Get DB conn
			conn, err = Conn(ctx)
			if err != nil {
				return err
			}
		}

		// Start transaction
		tx, err = conn.BeginTx(ctx, nil)
		if err != nil {
			return err
		}
		return nil
	}

	// Begin transaction
	var err error
	// No retry logic
	if b == nil {
		err = begin()
	} else {
		err = backoff.Retry(begin, b)
	}
	if err != nil {
		return err
	}

	// Rollback transaction
	// If no error encountered, transaction will be commited and this
	// rollback execution will be ignored
	defer tx.Rollback()

	// Execute callback
	if err := callback(tx); err != nil {
		return err
	}

	// Commit transaction
	return tx.Commit()
}

// DefaultBackOff returns the default backoff configuration based on Azure best practices
//
// Reference: https://docs.microsoft.com/en-us/azure/postgresql/concepts-connectivity#handling-transient-errors
func DefaultBackOff(maxRetries uint64) backoff.BackOff {
	b := backoff.NewExponentialBackOff()
	// 1. Wait for 5 seconds before your first retry. (for simplicity, we're just using backoff.InitialInterval to simulate)
	b.InitialInterval = 5 * time.Second
	b.RandomizationFactor = 0
	// 2. For each following retry, the increase the wait exponentially, up to 60 seconds.
	b.MaxElapsedTime = time.Minute
	// 3. Set a max number of retries at which point your application considers the operation failed.
	return backoff.WithMaxRetries(b, maxRetries)
}
