package db

import (
	"context"
	"net/http"
	"os"
)

type contextKey string

const txContextKey = contextKey("tx")

// TxFromRequest returns Transactor inside request Context.
func TxFromRequest(r *http.Request) Transactor {
	return TxFromContext(r.Context())
}

// TxFromContext returns Transactor inside Context.
func TxFromContext(c context.Context) Transactor {
	if value, ok := c.Value(txContextKey).(Transactor); ok {
		return value
	}
	return nil
}

// =====================
// FOR USE IN TESTS ONLY
// =====================

// ProvidesTxToTestRequest puts Transactor inside a test request Context.
// This is ONLY meant for tests.
func ProvidesTxToTestRequest(tx Transactor, r *http.Request) *http.Request {
	ctx := ProvidesTxToTestContext(tx, r.Context())
	return r.WithContext(ctx)
}

// ProvidesTxToTestContext puts Transactor inside a test Context.
// This is ONLY meant for tests.
func ProvidesTxToTestContext(tx Transactor, ctx context.Context) context.Context {
	if os.Getenv("APPENV") != "test" {
		panic("NOT for use in APPENV=" + os.Getenv("APPENV"))
	}
	newCtx := context.WithValue(ctx, txContextKey, tx)
	return newCtx
}
