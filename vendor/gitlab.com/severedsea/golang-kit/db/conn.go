package db

import (
	"context"
	"net/url"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq" // for `database/sql`
	"github.com/pkg/errors"
	"golang.org/x/sync/errgroup"
)

var (
	conn BeginnerExecutor
	mu   sync.Mutex
)

// Conn opens a default connection and returns a BeginnerExecutor
// Note: Please use `db.Tx` or `db.Transaction` whenever possible
func Conn(ctx context.Context) (BeginnerExecutor, error) {
	mu.Lock()
	defer mu.Unlock()

	if conn == nil {
		d, err := sqlx.Connect("postgres", os.Getenv("DATABASE_URL"))
		if err != nil {
			return nil, err
		}
		c := &DB{d.DB}

		// Extract database config from environment
		cfg, err := connConfigFromEnv()
		if err != nil {
			return nil, errors.Wrap(err, "connConfigFromEnv")
		}
		if cfg.ConnMaxLifetime > 0 {
			c.SetConnMaxLifetime(cfg.ConnMaxLifetime)
		}
		if cfg.MaxOpenConns > 0 {
			c.SetMaxOpenConns(cfg.MaxOpenConns)
		}
		if cfg.MaxIdleConns > 0 {
			c.SetMaxIdleConns(cfg.MaxIdleConns)
		}

		conn = c
	}

	return conn, nil
}

// connConfig is the struct representation for database connection configuration
type connConfig struct {
	MaxOpenConns    int
	MaxIdleConns    int
	ConnMaxLifetime time.Duration
}

// connConfigFromEnv returns the configured DB connection settings from the environment variables or the database string
//
// Extracts database connection configuration from various env vars/value based on the following priority (highest to lowest)
// DB pool size (max open conns)
// 1. DB_POOL env var
// 2. DATABASE_URL ?pool=x query param
// 3. database/sql default value (0 or unlimited)
//
// Max idle connections
// 1. DB_CONN_MAX_IDLE env var
// 2. Same value as DB_POOL
// 3. database/sql default value (2)
//
// Connection max lifetime duration (in time.Duration format)
// 1. DB_CONN_MAX_LIFETIME_DURATION
// 2. database/sql default value  (0 or forever)
func connConfigFromEnv() (connConfig, error) {
	var cfg connConfig

	// Get max open connections from DB string
	uri, err := url.Parse(os.Getenv("DATABASE_URL"))
	if err != nil {
		return connConfig{}, err
	}
	if p := strings.TrimSpace(uri.Query().Get("pool")); p != "" {
		cfg.MaxOpenConns, err = strconv.Atoi(p)
		if err != nil {
			return connConfig{}, err
		}
	}

	// Override configured open connections with environment variable, if available
	if p := strings.TrimSpace(os.Getenv("DB_POOL")); p != "" {
		cfg.MaxOpenConns, err = strconv.Atoi(p)
		if err != nil {
			return connConfig{}, err
		}
	}

	// Default idle connections to open connections
	cfg.MaxIdleConns = cfg.MaxOpenConns

	// Override default idle connections size with environment variable, if available
	if i := strings.TrimSpace(os.Getenv("DB_CONN_MAX_IDLE")); i != "" {
		cfg.MaxIdleConns, err = strconv.Atoi(i)
		if err != nil {
			return connConfig{}, err
		}
	}

	// Configure connection max lifetime with environment variable, if available
	if d := strings.TrimSpace(os.Getenv("DB_CONN_MAX_LIFETIME_DURATION")); d != "" {
		cfg.ConnMaxLifetime, err = time.ParseDuration(d)
		if err != nil {
			return connConfig{}, err
		}
	}

	return cfg, nil
}

// OpenConnections attempts to open the number of connections
func OpenConnections(ctx context.Context, d BeginnerExecutor, connCount int) error {
	g := errgroup.Group{}

	var wg sync.WaitGroup
	wg.Add(connCount)

	for i := connCount; i > 0; i-- {
		g.Go(func() error {
			tx, err := d.BeginTx(ctx, nil)
			if err != nil {
				wg.Done()
				return errors.Wrapf(err, "begin tx")
			}
			defer tx.Rollback()

			_, err = tx.Exec("SELECT 1")
			if err != nil {
				wg.Done()
				return errors.Wrapf(err, "select 1")
			}
			wg.Done()

			// wait until all other goroutines have started a new
			// transaction and executed the `SELECT 1` query
			// before we end the transaction in this goroutine.
			wg.Wait()
			return nil
		})
	}

	return g.Wait()
}
